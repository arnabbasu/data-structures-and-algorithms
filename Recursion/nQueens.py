def n_queens(n, current_row=0, placement=None, all_results=None):
    # https://docs.python-guide.org/writing/gotchas/#mutable-default-arguments
    if not placement:
        placement = []
    if not all_results:
        all_results = []

    if current_row == n:
        all_results.append(placement.copy())  # shallow copy of list since we will be modifying it
        return all_results

    for col in range(n):  # try to put the queen in each possible column
        placement.append(col)
        if is_valid(placement, current_row):
            all_results = n_queens(n, current_row + 1, placement, all_results)
        placement.pop()
    return all_results


def is_valid(placement, current_row):
    for row, col in enumerate(placement):
        if row == current_row:
            continue
        col_diff = abs(placement[current_row] - col)
        row_diff = current_row - row
        if col_diff == 0 or col_diff == row_diff:
            return False
    return True


if __name__ == '__main__':
    print(n_queens(4))
    print(n_queens(5))

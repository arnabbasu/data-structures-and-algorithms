class SummationPuzzleSolver:
    u = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    def __init__(self, a, b, sm):
        self._a = a
        self._b = b
        self._sum = sm
        unique_chars = set()
        for c in self._a + self._b + self._sum:
            unique_chars.add(c)
        self._unique = ''.join(sorted(unique_chars))

    @staticmethod
    def _make_number(code, mapping):
        n = 0
        if mapping[code[0]] == 0:
            return 0
        for c in code:
            n = (n * 10) + mapping[c]
        return n

    def _make_mapping_dict(self, mapping_str):
        mapping = {}
        for i in range(len(self._unique)):
            mapping[self._unique[i]] = int(mapping_str[i])
        return mapping

    def _get_numbers(self, mapping):
        return self._make_number(self._a, mapping), self._make_number(self._b, mapping), self._make_number(self._sum,
                                                                                                           mapping)

    def _is_solved(self, soln):
        mapping = self._make_mapping_dict(soln)
        a, b, sm = self._get_numbers(mapping)
        if a + b == sm and a and b and sm:
            print(self._a + ' + ' + self._b + ' = ' + self._sum)
            print(str(a) + ' + ' + str(b) + ' = ' + str(sm))
            return True
        return False

    def _generate_possibility(self, length, soln, universe, ignore):
        for e in universe:
            if e in ignore:
                continue
            if length == 1:
                if self._is_solved(soln + e):
                    return soln
            else:
                # u = universe.copy()
                ignore.add(e)
                self._generate_possibility(length - 1, soln + e, universe, ignore)
                ignore.remove(e)

    def solve(self):
        self._generate_possibility(len(self._unique), '', SummationPuzzleSolver.u, set())


if __name__ == '__main__':
    # import timeit
    s = SummationPuzzleSolver('pot', 'pan', 'bib')
    # print(timeit.timeit(s.solve, number=1))
    s.solve()
    s = SummationPuzzleSolver('dog', 'cat', 'pig')
    s.solve()
    s = SummationPuzzleSolver('boy', 'girl', 'baby')
    s.solve()
    s = SummationPuzzleSolver('send', 'more', 'money')
    s.solve()

def generate_all_permutations(A, cur_index=0, result=None):
    if not result:
        result = []

    if cur_index == len(A) - 1:
        result.append(A.copy())
        return result

    for i in range(cur_index, len(A)):
        A[cur_index], A[i] = A[i], A[cur_index]
        result = generate_all_permutations(A, cur_index + 1, result)
        A[cur_index], A[i] = A[i], A[cur_index]

    return result


if __name__ == '__main__':
    print(generate_all_permutations([1, 2, 3, 4, 5]))

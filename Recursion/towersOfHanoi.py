class Pin:
    def __init__(self, name):
        self._rings = []
        self._name = str(name)

    def add(self, ring):
        self._rings.append(ring)

    def remove(self):
        return self._rings.pop()

    def move(self, to):
        self._rings[-1].move(self, to)

    def get_name(self):
        return self._name

    def __str__(self):
        return self._name + ': ' + ', '.join(str(e) for e in self._rings[::-1])


class Ring:
    def __init__(self, name):
        self._name = str(name)

    def move(self, frm, to):
        print('Moving ring {} from pin {} to pin {}'.format(self._name, frm.get_name(), to.get_name()))
        to.add(frm.remove())

    def __str__(self):
        return self._name


def print_pins(frm, to, via):
    print('----')
    print(str(frm))
    print(str(to))
    print(str(via))
    print('----')


def hanoi(num_rings, frm, to, via):
    if num_rings == 0:
        return
    hanoi(num_rings - 1, frm, via, to)
    frm.move(to)
    print_pins(frm, to, via)
    hanoi(num_rings - 1, via, to, frm)


if __name__ == '__main__':
    p1 = Pin('p1')
    p1.add(Ring('5'))
    p1.add(Ring('4'))
    p1.add(Ring('3'))
    p1.add(Ring('2'))
    p1.add(Ring('1'))
    p1.add(Ring('0'))
    p2 = Pin('p2')
    p3 = Pin('p3')
    hanoi(6, p1, p2, p3)
    print(str(p1))
    print(str(p2))
    print(str(p3))

import math

EMPTY = 0


def get_subgrid_len(grid_len):
    """ this holds for a 9X9 grid, not sure if this will always hold"""
    return int(math.sqrt(grid_len))


def get_subgrid_start_index(i, j, subgrid_len):
    sg_i = int((i // subgrid_len) * subgrid_len)
    sg_j = int((j // subgrid_len) * subgrid_len)
    return sg_i, sg_j


def can_add(grid, i, j, val):
    # check row and col
    for x in range(len(grid)):
        if grid[i][x] == val or grid[x][j] == val:
            return False

    # check sub-grid
    sg_len = get_subgrid_len(len(grid))
    sg_start_i, sg_start_j = get_subgrid_start_index(i, j, sg_len)
    for x in range(sg_start_i, sg_start_i + sg_len):
        for y in range(sg_start_j, sg_start_j + sg_len):
            if grid[x][y] == val:
                return False
    return True


def solve_sudoku(grid, i=0, j=0):
    if i >= len(grid):  # this row is done, move to next
        i, j = 0, j + 1
        if j >= len(grid):  # all rows are done
            return True, grid

    if grid[i][j] != EMPTY:  # this position is pre-filled
        return solve_sudoku(grid, i + 1, j)

    for val in range(0, len(grid) + 1):
        if can_add(grid, i, j, val):  # checking before populating the value is easier
            grid[i][j] = val
            can, grid = solve_sudoku(grid, i + 1, j)
            if can:
                return True, grid
        grid[i][j] = EMPTY  # we got here because putting val in this position does not lead to a valid solution

    return False, grid  # we got here because we ran out of values to put into grid[i][j]


if __name__ == '__main__':
    grid = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    can, grid = solve_sudoku(grid)
    print(grid)

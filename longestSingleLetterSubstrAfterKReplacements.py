# Given a string with lowercase letters only, if you are allowed to replace no more than ‘k’ letters with any letter,
# find the length of the longest substring having the same letters after replacement.


def longest_single_letter_substring(k, st):
    window_start, max_len = 0, 0
    occurrences_of = {}  # tracks the actual occurrences of the characters in the window
    max_occurrences = 0  # tracks the max_occurrences of a character we have seen in ANY window so far
    for window_end in range(len(st)):
        cur_letter = st[window_end]
        if cur_letter in occurrences_of:
            occurrences_of[cur_letter] += 1
        else:
            occurrences_of[cur_letter] = 1
        # by adding 1 to the occurrences of the cur letter we might have made it the most occurring letter
        max_occurrences = max(max_occurrences, occurrences_of[cur_letter])

        # the longest a substring is allowed to be is the max_occurrences in ANY window + the number of substitutions
        # allowed if the current length of the substring is longer than that then slide the window rather than allow
        # it to grow
        longest_allowed_substring = max_occurrences + k
        current_substring_len = window_end - window_start + 1
        if current_substring_len > longest_allowed_substring:
            left_letter = st[window_start]
            occurrences_of[left_letter] -= 1
            window_start += 1
        max_len = max(max_len, window_end - window_start + 1)

    return max_len


def main():
    print(longest_single_letter_substring(2, "aabccbb"))
    print(longest_single_letter_substring(1, "abbcb"))
    print(longest_single_letter_substring(1, "abccde"))
    print(longest_single_letter_substring(1, "abdcccc"))


main()

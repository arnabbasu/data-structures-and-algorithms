# Given a string, find the length of the longest substring which has no repeating characters.


def longest_substr_no_repeating_chars(st):
    window_start, max_len, chars_encountered_before = 0, 0, {}
    for window_end in range(len(st)):
        cur_char = st[window_end]
        # we have seen this character before but at this stage we don't know if it is still in the substring or not
        # since we don't delete anything from the map
        if cur_char in chars_encountered_before:
            # we need to take max because the character might have fallen out of the substring in which case
            # the stored index will be less than window_start
            window_start = max(window_start, chars_encountered_before[cur_char] + 1)
        chars_encountered_before[cur_char] = window_end  # update the index of the char
        max_len = max(max_len, window_end - window_start + 1)

    return max_len


def main():
    print("Length of the longest substring: " + str(longest_substr_no_repeating_chars("aabccbb")))
    print("Length of the longest substring: " + str(longest_substr_no_repeating_chars("abbbb")))
    print("Length of the longest substring: " + str(longest_substr_no_repeating_chars("abccde")))


main()

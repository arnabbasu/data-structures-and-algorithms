# Given an array of positive numbers and a positive number ‘k’, find the maximum sum of any contiguous subarray of
# size ‘k’.


def max_sum_subarray(sub_array_len, arr):
    if sub_array_len > len(arr):
        raise Exception('error')

    window_sum, window_start, max_sum = 0.0, 0, 0
    # expand window from left to right and calculate sum
    for window_end in range(sub_array_len):
        window_sum += arr[window_end]
        max_sum = window_sum  # init max_sum to the 1st sum

    # for the remaining elements of arr
    for window_end in range(sub_array_len, len(arr)):
        window_sum -= arr[window_start]  # subtract outgoing element
        window_sum += arr[window_end]  # add incoming element
        window_start += 1  # slide window
        max_sum = max(max_sum, window_sum)  # figure out if the new sum is greater than the old sum

    return max_sum


def main():
    print("Maximum sum of a subarray of size K: " + str(max_sum_subarray(3, [2, 1, 5, 1, 3, 2])))
    print("Maximum sum of a subarray of size K: " + str(max_sum_subarray(2, [2, 3, 4, 1, 5])))


main()

# Given a string and a list of words, find all the starting indices of substrings in the given string that are a
# concatenation of all the given words exactly once without any overlapping of words. You can assume that all words
# are of the same length.


def find_concatenated_substring(st, lst):
    # check for empty string and empty list
    words_master = {}
    start_indexes = []

    for word in lst:
        if word in words_master:
            words_master[word] += 1
        else:
            words_master[word] = 1

    num_words = len(words_master)
    word_length = len(lst[0])

    # we traverse over all possible start indexes
    for window_start in range(len(st) - (num_words * word_length) + 1):
        window_length = word_length
        cur_word = st[window_start:(window_start + window_length)]
        matched = 0
        words = words_master.copy()
        # while current word is in substring and window length is not greater than the length of the required substring
        # length check is there so that we don't keep checking if one word of the list is repeating over and over
        while cur_word in words and window_length <= (len(lst) * word_length):
            words[cur_word] -= 1
            if words[cur_word] == 0:  # this word has been found the right number of times
                matched += 1
                if matched == num_words:  # all words have been found
                    start_indexes.append(window_start)
                    break
            # extract the next word
            cur_word = st[(window_start + window_length):(window_start + window_length + word_length)]
            window_length = window_length + word_length

    return start_indexes


# The time complexity of the above algorithm will be O(N∗M∗Len) where ‘N’ is the number of characters in
# the given string, ‘M’ is the total number of words, and ‘Len’ is the length of a word.

# The space complexity of the algorithm is O(M) since at most, we will be storing all the words in the two HashMaps.
# In the worst case, we also need O(N) space for the resulting list. So, the overall space complexity of the
# algorithm will be O(M+N).

def main():
    print(find_concatenated_substring("catfoxcat", ["cat", "fox"]))  # [0, 3]
    print(find_concatenated_substring("catcatfoxfox", ["cat", "fox"]))  # [3]
    print(find_concatenated_substring("barfoothefoobarman", ["foo", "bar"]))  # [0, 9]
    print(find_concatenated_substring("catbatatecatatebat", ["cat", "ate", "bat"]))  # [0, 3, 9]
    print(find_concatenated_substring("abcdababcd", ["ab", "ab", "cd"]))  # [0, 2, 4]
    print(find_concatenated_substring("abcdababcd", ["ab", "ab"]))  # [4]


main()

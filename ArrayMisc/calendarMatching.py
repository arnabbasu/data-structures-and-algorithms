# Calendar Matching
#
# Imagine that you want to schedule a meeting of a certain duration with a co-worker. You have access to your calendar
# and your coworker's calendar (both of which contain your respective meetings for the day,
# in the form of [startTime, endTime]), as well as both of your daily bounds (i.e., the earliest and latest times at
# which you're available for meetings every day, in the form of [earliestTime, latestTime]). Write a function that
# takes in your calendar, your daily bounds, your coworker's calendar, your coworker's daily bounds, and the duration of
# the meeting that you want to schedule, and that returns a list of all the time blocks (in the form of [startTime,
# endTime]) during which you could schedule the meeting. Note that times will be given and should be returned in
# military time (example times: '8:30', '9:01', '23:56').
#
# Sample input:
# [['9:00', '10:30'], ['12:00', '13:00'], ['16:00', '18:00']]
# ['9:00', '20:00']
# [['10:00', '11:30'], ['12:30', '14:30'], ['14:30', '15:00'], ['16:00', '17:00']]
# ['10:00', '18:30']
# 30
# Sample Output: [['11:30', '12:00'], ['15:00', '16:00'], ['18:00', '18:30']]

# Add, edit, or remove tests in this file.
# Treat it as your playground!


import unittest
from datetime import datetime
from datetime import timedelta


def merge_intervals(i1, i2):
    return [min(i1[0], i2[0]), max(i1[1], i2[1])]


def calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration):
    bounds = [max(datetime.strptime(daily_bounds1[0], "%H:%M"), datetime.strptime(daily_bounds2[0], "%H:%M")),
              min(datetime.strptime(daily_bounds1[1], "%H:%M"), datetime.strptime(daily_bounds2[1], "%H:%M"))]
    cal1 = [[datetime.strptime(c[0], "%H:%M"), datetime.strptime(c[1], "%H:%M")] for c in calendar1]
    cal1.insert(0, [datetime.strptime("0:00", "%H:%M"), bounds[0]])
    cal1.append([bounds[1], datetime.strptime("23:59", "%H:%M")])
    cal2 = [[datetime.strptime(c[0], "%H:%M"), datetime.strptime(c[1], "%H:%M")] for c in calendar2]
    cal2.insert(0, [datetime.strptime("0:00", "%H:%M"), bounds[0]])
    cal2.append([bounds[1], datetime.strptime("23:59", "%H:%M")])

    master_cal = []
    c1i = 0
    c2i = 0

    while c1i < len(cal1) and c2i < len(cal2):
        if cal1[c1i][1] < cal2[c2i][0]:
            if len(master_cal) == 0 or (len(master_cal) > 0 and master_cal[-1][1] < cal1[c1i][0]):
                master_cal.append(cal1[c1i])
            else:
                ival = merge_intervals(master_cal[-1], cal1[c1i])
                master_cal[-1][0] = ival[0]
                master_cal[-1][1] = ival[1]
            c1i = c1i + 1
        elif cal2[c2i][1] < cal1[c1i][0]:
            if len(master_cal) == 0 or (len(master_cal) > 0 and master_cal[-1][1] < cal2[c2i][0]):
                master_cal.append(cal2[c2i])
            else:
                ival = merge_intervals(master_cal[-1], cal2[c2i])
                master_cal[-1][0] = ival[0]
                master_cal[-1][1] = ival[1]
            c2i = c2i + 1
        else:
            ival = merge_intervals(cal1[c1i], cal2[c2i])
            if len(master_cal) == 0 or (len(master_cal) > 0 and master_cal[-1][1] < ival[0]):
                master_cal.append(ival)
            else:
                ival = merge_intervals(master_cal[-1], ival)
                master_cal[-1][0] = ival[0]
                master_cal[-1][1] = ival[1]
            c1i = c1i + 1
            c2i = c2i + 1
    while c1i < len(cal1):
        if len(master_cal) == 0 or (len(master_cal) > 0 and master_cal[-1][1] < cal1[c1i][0]):
            master_cal.append(cal1[c1i])
        else:
            ival = merge_intervals(master_cal[-1], cal1[c1i])
            master_cal[-1][0] = ival[0]
            master_cal[-1][1] = ival[1]
        c1i = c1i + 1
    while c2i < len(cal2):
        if len(master_cal) == 0 or (len(master_cal) > 0 and master_cal[-1][1] < cal2[c2i][0]):
            master_cal.append(cal2[c2i])
        else:
            ival = merge_intervals(master_cal[-1], cal2[c2i])
            master_cal[-1][0] = ival[0]
            master_cal[-1][1] = ival[1]
        c2i = c2i + 1

    mcs = 0
    mce = len(master_cal) - 1
    available = []
    dur = timedelta(minutes=meeting_duration)

    if mcs > mce and bounds[1] - bounds[0] >= dur:
        available = [[bounds[0], bounds[1]]]
    else:
        for i in range(mcs, mce):
            if (master_cal[i + 1][0] - master_cal[i][1]) >= dur:
                available.append([master_cal[i][1], master_cal[i + 1][0]])

    return [[t[0].strftime("%#H:%M"), t[1].strftime("%#H:%M")] for t in available]


class TestProgram(unittest.TestCase):
    def test_case_1(self):
        calendar1 = [["9:00", "10:30"], ["12:00", "13:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["9:00", "20:00"]
        calendar2 = [["10:00", "11:30"], ["12:30", "14:30"], ["14:30", "15:00"], ["16:00", "17:00"]]
        daily_bounds2 = ["10:00", "18:30"]
        meeting_duration = 30
        expected = [["11:30", "12:00"], ["15:00", "16:00"], ["18:00", "18:30"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_2(self):
        calendar1 = [["9:00", "10:30"], ["12:00", "13:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["9:00", "20:00"]
        calendar2 = [["10:00", "11:30"], ["12:30", "14:30"], ["14:30", "15:00"], ["16:00", "17:00"]]
        daily_bounds2 = ["10:00", "18:30"]
        meeting_duration = 45
        expected = [["15:00", "16:00"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_3(self):
        calendar1 = [["9:00", "10:30"], ["12:00", "13:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["8:00", "20:00"]
        calendar2 = [["10:00", "11:30"], ["12:30", "14:30"], ["14:30", "15:00"], ["16:00", "17:00"]]
        daily_bounds2 = ["7:00", "18:30"]
        meeting_duration = 45
        expected = [["8:00", "9:00"], ["15:00", "16:00"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_4(self):
        calendar1 = [["8:00", "10:30"], ["11:30", "13:00"], ["14:00", "16:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["8:00", "18:00"]
        calendar2 = [["10:00", "11:30"], ["12:30", "14:30"], ["14:30", "15:00"], ["16:00", "17:00"]]
        daily_bounds2 = ["7:00", "18:30"]
        meeting_duration = 15
        expected = []
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_5(self):
        calendar1 = [["10:00", "10:30"], ["10:45", "11:15"], ["11:30", "13:00"], ["14:15", "16:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["9:30", "20:00"]
        calendar2 = [["10:00", "11:00"], ["12:30", "13:30"], ["14:30", "15:00"], ["16:00", "17:00"]]
        daily_bounds2 = ["9:00", "18:30"]
        meeting_duration = 15
        expected = [["9:30", "10:00"], ["11:15", "11:30"], ["13:30", "14:15"], ["18:00", "18:30"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_6(self):
        calendar1 = [["10:00", "10:30"], ["10:45", "11:15"], ["11:30", "13:00"], ["14:15", "16:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["9:30", "20:00"]
        calendar2 = [["10:00", "11:00"], ["10:30", "20:30"]]
        daily_bounds2 = ["9:00", "22:30"]
        meeting_duration = 60
        expected = []
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_7(self):
        calendar1 = [["10:00", "10:30"], ["10:45", "11:15"], ["11:30", "13:00"], ["14:15", "16:00"], ["16:00", "18:00"]]
        daily_bounds1 = ["9:30", "20:00"]
        calendar2 = [["10:00", "11:00"], ["10:30", "16:30"]]
        daily_bounds2 = ["9:00", "22:30"]
        meeting_duration = 60
        expected = [["18:00", "20:00"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_8(self):
        calendar1 = []
        daily_bounds1 = ["9:30", "20:00"]
        calendar2 = []
        daily_bounds2 = ["9:00", "16:30"]
        meeting_duration = 60
        expected = [["9:30", "16:30"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_9(self):
        calendar1 = []
        daily_bounds1 = ["9:00", "16:30"]
        calendar2 = []
        daily_bounds2 = ["9:30", "20:00"]
        meeting_duration = 60
        expected = [["9:30", "16:30"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_10(self):
        calendar1 = []
        daily_bounds1 = ["9:30", "16:30"]
        calendar2 = []
        daily_bounds2 = ["9:30", "16:30"]
        meeting_duration = 60
        expected = [["9:30", "16:30"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)

    def test_case_11(self):
        calendar1 = [
            ["7:00", "7:45"],
            ["8:15", "8:30"],
            ["9:00", "10:30"],
            ["12:00", "14:00"],
            ["14:00", "15:00"],
            ["15:15", "15:30"],
            ["16:30", "18:30"],
            ["20:00", "21:00"],
        ]
        daily_bounds1 = ["6:30", "22:00"]
        calendar2 = [["9:00", "10:00"], ["11:15", "11:30"], ["11:45", "17:00"], ["17:30", "19:00"], ["20:00", "22:15"]]
        daily_bounds2 = ["8:00", "22:30"]
        meeting_duration = 30
        expected = [["8:30", "9:00"], ["10:30", "11:15"], ["19:00", "20:00"]]
        result = calendar_matching(calendar1, daily_bounds1, calendar2, daily_bounds2, meeting_duration)
        self.assertEqual(result, expected)


if __name__ == "__main__":
    unittest.main()

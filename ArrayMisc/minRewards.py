# There are N children standing in a line. Each child is assigned a rating value.
#
# You are giving candies to these children subjected to the following requirements:
#
# - Each child must have at least one candy.
# - Children with a higher rating get more candies than their neighbors.
# What is the minimum candies you must give?


import unittest


def min_rewards(scores):
    rewards = [1 for _ in range(len(scores))]

    for i in range(1, len(scores)):
        if scores[i] > scores[i - 1]:
            rewards[i] = rewards[i - 1] + 1

    # for array of len 5 we will get 3, 2, 1, 0
    # so we will compare s[4] to s[3], s[3] to s[2] etc and we are
    # looking for increasing order, i.e. current s[i] is > than s[i+1]
    for i in reversed(range(len(scores) - 1)):
        if scores[i] > scores[i + 1]:
            rewards[i] = max(rewards[i], rewards[i + 1] + 1)

    return sum(rewards)


class TestProgram(unittest.TestCase):
    def test_case_1(self):
        self.assertEqual(min_rewards([1]), 1)

    def test_case_2(self):
        self.assertEqual(min_rewards([5, 10]), 3)

    def test_case_3(self):
        self.assertEqual(min_rewards([10, 5]), 3)

    def test_case_4(self):
        self.assertEqual(min_rewards([4, 2, 1, 3]), 8)

    def test_case_5(self):
        self.assertEqual(min_rewards([0, 4, 2, 1, 3]), 9)

    def test_case_6(self):
        self.assertEqual(min_rewards([8, 4, 2, 1, 3, 6, 7, 9, 5]), 25)

    def test_case_7(self):
        self.assertEqual(min_rewards([2, 20, 13, 12, 11, 8, 4, 3, 1, 5, 6, 7, 9, 0]), 52)

    def test_case_8(self):
        self.assertEqual(min_rewards([2, 1, 4, 3, 6, 5, 8, 7, 10, 9]), 15)

    def test_case_9(self):
        self.assertEqual(
            min_rewards(
                [
                    800,
                    400,
                    20,
                    10,
                    30,
                    61,
                    70,
                    90,
                    17,
                    21,
                    22,
                    13,
                    12,
                    11,
                    8,
                    4,
                    2,
                    1,
                    3,
                    6,
                    7,
                    9,
                    0,
                    68,
                    55,
                    67,
                    57,
                    60,
                    51,
                    661,
                    50,
                    65,
                    53,
                ]
            ),
            93,
        )


if __name__ == "__main__":
    unittest.main()

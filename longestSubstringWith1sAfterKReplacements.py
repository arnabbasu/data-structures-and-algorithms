# Given an array containing 0s and 1s, if you are allowed to replace no more than ‘k’ 0s with 1s, find the length of
# the longest subarray having all 1s


def longest_substring_of_1s(k, st):
    window_start, max_len = 0, 0
    max_1s = 0
    # Try to extend the range [window_start, window_end]
    for window_end in range(len(st)):
        if st[window_end] == 1:
            max_1s += 1

        # In current_substring_len we have a maximum of 1s repeating 'max_ones_count' times, this means we can have a
        # window with 'max_ones_count' 1s and the remaining are 0s which should replace with 1s.
        # now, if the remaining 0s are more than 'k', it is the time to shrink the window as we are not allowed to
        # replace more than 'k' 0s
        longest_possible_substring = max_1s + k
        current_substring_len = window_end - window_start + 1
        if current_substring_len > longest_possible_substring:
            if st[window_start] == 1:
                max_1s -= 1
            window_start += 1
        max_len = max(max_len, window_end - window_start + 1)

    return max_len


def main():
    print(longest_substring_of_1s(2, [0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1]))
    print(longest_substring_of_1s(3, [0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1]))


main()

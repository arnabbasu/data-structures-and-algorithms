# Given a binary tree, find the root-to-leaf path with the maximum sum


from typing import List, Tuple


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def find_path_internal(root: 'TreeNode', path: List[int], result: List[int], cmx: int, s: int) -> Tuple[int, List[int]]:
    if root is None:
        return cmx, result

    if root.left is None and root.right is None and root.val + s > cmx:
        path = path + [root.val]
        cmx = s + root.val
        return cmx, path

    cmx, result = find_path_internal(root.left, path + [root.val], result, cmx, s + root.val)
    return find_path_internal(root.right, path + [root.val], result, cmx, s + root.val)


def find_path(root: 'TreeNode') -> Tuple[int, List[int]]:
    return find_path_internal(root, [], [], 0, 0)


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(4)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    m, p = find_path(root)
    print("Tree paths with max sum " + str(m) + ": " + str(p))


main()

# Given a binary tree, return all root-to-leaf paths

from typing import List, Optional


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def find_paths_internal(root: Optional['TreeNode'], paths: List[List[int]], current_path: List[int]) -> None:
    if root is None:
        return

    if root.right is None and root.left is None:
        paths.append(current_path + [root.val])
        return

    find_paths_internal(root.left, paths, current_path + [root.val])
    find_paths_internal(root.right, paths, current_path + [root.val])
    return


def find_paths(root: Optional['TreeNode']) -> List[List[int]]:
    all_paths = []
    find_paths_internal(root, all_paths, [])
    return all_paths


def main():
    print("Tree paths: " + str(find_paths(None)))
    root = TreeNode(12)
    print("Tree paths: " + str(find_paths(root)))
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    print("Tree paths: " + str(find_paths(root)))
    root.left.left = TreeNode(4)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Tree paths: " + str(find_paths(root)))


main()

# Given a binary tree and a number sequence, find if the sequence is present as a root-to-leaf path in the given tree.

from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def find_path_internal(root: 'TreeNode', sequence: List[int], current_index: int) -> bool:
    if root is None:
        if current_index < len(sequence):
            return False
        else:
            return True
    else:
        if root.val != sequence[current_index]:
            return False
        else:
            return find_path_internal(root.left, sequence, current_index + 1) or find_path_internal(root.right,
                                                                                                    sequence,
                                                                                                    current_index + 1)


def find_path(root: 'TreeNode', sequence: List[int]) -> bool:
    return find_path_internal(root, sequence, 0)


def main():
    root = TreeNode(1)
    root.left = TreeNode(0)
    root.right = TreeNode(1)
    root.left.left = TreeNode(1)
    root.right.left = TreeNode(6)
    root.right.right = TreeNode(5)

    print("Tree has path sequence: " + str(find_path(root, [1, 0, 7])))
    print("Tree has path sequence: " + str(find_path(root, [1, 1, 6])))

    root = TreeNode(1)
    root.left = TreeNode(1)
    root.right = TreeNode(1)
    root.left.left = TreeNode(1)
    root.left.left = TreeNode(1)
    root.right.left = TreeNode(6)
    root.right.right = TreeNode(5)

    print("Tree has path sequence: " + str(find_path(root, [1, 1, 6])))


main()

# Given a binary tree and a number ‘S’, find all paths from root-to-leaf such that the sum of all the node values of
# each path equals ‘S’

from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def find_paths_internal(root: 'TreeNode', s: int, paths: List[List[int]], path: List[int]) -> None:
    if root is None:
        return

    if root.left is None and root.right is None and root.val == s:
        paths.append(path + [root.val])
        return

    find_paths_internal(root.left, s - root.val, paths, path + [root.val])
    find_paths_internal(root.right, s - root.val, paths, path + [root.val])
    return


def find_paths(root: 'TreeNode', s: int) -> List[List[int]]:
    all_paths: List[List[int]] = []
    find_paths_internal(root, s, all_paths, [])
    return all_paths


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(4)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    s = 23
    print("Tree paths with sum " + str(s) +
          ": " + str(find_paths(root, s)))


main()

# Given a binary tree where each node can only have a digit (0-9) value, each root-to-leaf path will represent a
# number. Find the total sum of all the numbers represented by all paths.


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def find_sum_of_path_numbers_internal(root: 'TreeNode', current_sum: int, current_num: int) -> int:
    if root is None:
        return current_sum

    current_num = (current_num * 10) + root.val

    if root.left is None and root.right is None:
        return current_sum + current_num

    current_sum = find_sum_of_path_numbers_internal(root.left, current_sum, current_num)
    return find_sum_of_path_numbers_internal(root.right, current_sum, current_num)


def find_sum_of_path_numbers(root: 'TreeNode') -> int:
    return find_sum_of_path_numbers_internal(root, 0, 0)


def main():
    root = TreeNode(1)
    root.left = TreeNode(0)
    root.right = TreeNode(1)
    root.left.left = TreeNode(1)
    root.right.left = TreeNode(6)
    root.right.right = TreeNode(5)
    print("Total Sum of Path Numbers: " + str(find_sum_of_path_numbers(root)))


main()

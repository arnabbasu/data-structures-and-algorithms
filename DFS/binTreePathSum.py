# Given a binary tree and a number ‘S’, find if the tree has a path from root-to-leaf such that the sum of all the
# node values of that path equals ‘S’


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def has_path(root: 'TreeNode', s: int) -> bool:
    if root is None:
        if sum == 0:
            return True
        else:
            return False

    return has_path(root.left, s - root.val) or has_path(root.right, s - root.val)


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Tree has path: " + str(has_path(root, 23)))
    print("Tree has path: " + str(has_path(root, 16)))


main()

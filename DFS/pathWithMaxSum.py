# Find the path with the maximum sum in a given binary tree. Write a function that returns the maximum sum.
# A path can be defined as a sequence of nodes between any two nodes and doesnt necessarily pass through the root

import math


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


class MaxSum:
    def __init__(self: 'MaxSum'):
        self.max_sum = -math.inf

    def find_subtree_and_max_path_sum(self: 'MaxSum', root: 'TreeNode') -> int:
        if root is None:
            return 0

        # ignore any subtree sums that reduce the overall path sum
        left_subtree_sum = max(self.find_subtree_and_max_path_sum(root.left), 0)
        right_subtree_sum = max(self.find_subtree_and_max_path_sum(root.right), 0)

        self.max_sum = max(self.max_sum, left_subtree_sum + right_subtree_sum + root.val)

        return max(left_subtree_sum, right_subtree_sum) + root.val

    def find_maximum_path_sum(self: 'MaxSum', root: 'TreeNode') -> int:
        self.find_subtree_and_max_path_sum(root)
        return int(self.max_sum)


def find_maximum_path_sum(root):
    ms = MaxSum()
    return ms.find_maximum_path_sum(root)


def main():
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)

    print("Maximum Path Sum: " + str(find_maximum_path_sum(root)))
    root.left.left = TreeNode(1)
    root.left.right = TreeNode(3)
    root.right.left = TreeNode(5)
    root.right.right = TreeNode(6)
    root.right.left.left = TreeNode(7)
    root.right.left.right = TreeNode(8)
    root.right.right.left = TreeNode(9)
    print("Maximum Path Sum: " + str(find_maximum_path_sum(root)))

    root = TreeNode(-1)
    root.left = TreeNode(-3)
    print("Maximum Path Sum: " + str(find_maximum_path_sum(root)))


main()

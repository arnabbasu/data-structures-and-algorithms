# Given a binary tree and a number ‘S’, find all paths in the tree such that the sum of all the node values of each
# path equals ‘S’. Please note that the paths can start or end at any node but all paths must follow direction from
# parent to child (top to bottom)

from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int, left: 'TreeNode' = None, right: 'TreeNode' = None):
        self.val = val
        self.left = left
        self.right = right


def count_paths_internal(root: 'TreeNode', s: int, sum_above: List[int], count: int) -> int:
    if root is None:
        return count
    for sm in sum_above:
        if sm + root.val == s:
            count = count + 1
    if root.val == s:
        count = count + 1
    sum_above.append(0)
    count = count_paths_internal(root.left, s, [x + root.val for x in sum_above], count)
    count = count_paths_internal(root.right, s, [x + root.val for x in sum_above], count)
    return count


def count_paths(root: 'TreeNode', s: int) -> int:
    count = 0
    if root is None:
        return count
    if root.val == s:
        count = count + 1
    count = count_paths_internal(root.left, s, [root.val], count)
    count = count_paths_internal(root.right, s, [root.val], count)
    return count


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(4)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Tree has paths: " + str(count_paths(root, 11)))
    root = TreeNode(1)
    print("Tree has paths: " + str(count_paths(root, 1)))
    root = TreeNode(1)
    root.left = TreeNode(2)
    print("Tree has paths: " + str(count_paths(root, 2)))
    root = TreeNode(0)
    root.left = TreeNode(1)
    root.right = TreeNode(1)
    print("Tree has paths: " + str(count_paths(root, 1)))
    root = TreeNode(1)
    root.right = TreeNode(2)
    root.right.right = TreeNode(3)
    root.right.right.right = TreeNode(4)
    root.right.right.right.right = TreeNode(5)
    print("Tree has paths: " + str(count_paths(root, 3)))


main()

# Given an array with positive numbers and a target number, find all subarrays in it whose product is less than the
# target number
from typing import List
from collections import deque


def find_subarrays_prod_less_than(arr: List[int], target: int) -> List[List[int]]:
    result = []
    product, left = 1, 0
    for right in range(len(arr)):
        product *= arr[right]
        while product >= target and left <= right:
            product /= arr[left]
            left += 1
        # if all elements in the subarray are larger than target, left will be > right at this point
        if left > right:
            continue
        # since the subarray left..right has product less than target, all subarrays in left..right will have product
        # less than target
        # to prevent duplicate subarrays we grow the list of subarrays from the right
        temp = deque()
        for i in range(right, left - 1, -1):
            temp.appendleft(arr[i])
            result.append(list(temp))
    return result


# outer loop runs in O(n), finding all subarrays takes O(n^2). overall O(n^3)
# Space: O(n^2)
#   all subarrays between 0 and len(arr) - 1 = n
#   all subarrays between 1 and len(arr) - 1 = n - 1
#   ....
#   all subarrays between len(arr) - 1 and len(arr) - 1 = 1
#   giving: n + (n -1) + (n - 2) + .... + 2 + 1 = (n^2)


def main():
    print(find_subarrays_prod_less_than([2, 5, 3, 10], 30))
    print(find_subarrays_prod_less_than([8, 2, 6, 5], 50))
    print(find_subarrays_prod_less_than([30, 35, 3, 10], 30))
    print(find_subarrays_prod_less_than([2, 5, 3, 10], 301))


main()

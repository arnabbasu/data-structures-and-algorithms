# Given an unsorted array of numbers and a target ‘key’, remove all instances of ‘key’ in-place and return the
# new length of the array


def remove_element(arr, elem):
    keyless_arr_end = 0
    for read_idx, read_elem in enumerate(arr):
        if read_elem != elem:
            arr[keyless_arr_end] = read_elem
            keyless_arr_end += 1

    return keyless_arr_end


def main():
    print("Array new length: " + str(remove_element([3, 2, 3, 6, 3, 10, 9, 3], 3)))
    print("Array new length: " + str(remove_element([2, 11, 2, 2, 1], 2)))


main()

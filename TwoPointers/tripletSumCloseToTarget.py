# Given an array of unsorted numbers and a target number, find a triplet in the array whose sum is as close to the
# target number as possible
from typing import List
import math


# outer loop runs in O(n) calls pair_sum_... which runs in O(n), sorting in O(nlogn). Overall O(n^2)
def triplet_sum_close_to_target(arr: List[int], target_sum: int) -> int:
    arr.sort()
    difference_from_target = math.inf
    for idx in range(len(arr) - 2):
        pair_sum_difference = pair_sum_close_to_target(arr, idx + 1, -arr[idx] + target_sum)
        if abs(pair_sum_difference) < abs(difference_from_target):
            difference_from_target = pair_sum_difference
    return target_sum - difference_from_target


def pair_sum_close_to_target(arr: List[int], left_idx: int, target_sum: int) -> int:
    min_difference_from_target = math.inf
    right_idx = len(arr) - 1
    while left_idx < right_idx:
        current_sum = arr[left_idx] + arr[right_idx]
        difference_from_target = target_sum - current_sum
        if abs(difference_from_target) < abs(min_difference_from_target):
            min_difference_from_target = difference_from_target
        if difference_from_target == 0:
            return 0
        elif current_sum < target_sum:
            left_idx += 1
        else:
            right_idx -= 1
    return min_difference_from_target


def main():
    print(triplet_sum_close_to_target([-2, 0, 1, 2], 2))
    print(triplet_sum_close_to_target([-3, -1, 1, 2], 1))
    print(triplet_sum_close_to_target([1, 0, 1, 1], 100))
    print(triplet_sum_close_to_target([-1, 0, -1, -1], 100))
    print(triplet_sum_close_to_target([1, 0, 1, 1], 3))
    print(triplet_sum_close_to_target([-1, 2, 1, -4], 1))


main()

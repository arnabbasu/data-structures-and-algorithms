# Given two strings containing backspaces (identified by the character ‘#’), check if the two strings are equal.
# O(len(s1) + len(s2))


def backspace_compare(str1: str, str2: str) -> bool:
    index1 = len(str1) - 1
    index2 = len(str2) - 1

    while index1 >= 0 or index2 >= 0:  # one string can be longer than the other
        index1 = get_next_valid_index(str1, index1)
        index2 = get_next_valid_index(str2, index2)

        if index1 < 0 and index2 < 0:  # both strings have ended (string starts with #)
            return True
        if index1 < 0 or index2 < 0:  # one string ended other did not
            return False
        if str1[index1] != str2[index2]:
            return False

        index1 -= 1
        index2 -= 1

    return True


def get_next_valid_index(s: str, idx: int) -> int:
    # count backspaces from end of string and skip those many characters
    backspace_count = 0
    while idx >= 0:
        if s[idx] == "#":
            backspace_count += 1
        elif backspace_count > 0:
            backspace_count -= 1
        else:
            break
        idx -= 1
    return idx


def main():
    print(backspace_compare("xy#z", "xzz#") == True)
    print(backspace_compare("xy#z", "xyz#") == False)
    print(backspace_compare("xp#", "xyz##") == True)
    print(backspace_compare("#xp", "xyz##") == False)
    print(backspace_compare("#xp", "#xp") == True)
    print(backspace_compare("", "xyz##") == False)
    print(backspace_compare("", "") == True)
    print(backspace_compare("xywrrmp", "xywrrmu#p") == True)


main()

# Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all
# unique quadruplets in the array which gives the sum of target.

import unittest


def four_number_sum(array, target_sum):
    sums = {}
    result = []
    for i in range(len(array) - 1):
        for j in range(i + 1, len(array)):
            diff = target_sum - (array[i] + array[j])
            if diff in sums:
                for pair in sums[diff]:
                    result.append(pair + [array[i], array[j]])

        # Adding only sums where the current i is the second number prevents double counting
        for k in range(0, i):
            s = array[i] + array[k]
            if s in sums:
                sums[s].append([array[k], array[i]])
            else:
                sums[s] = [[array[k], array[i]]]

    return result


def sort_and_stringify(array):
    return ",".join(sorted(list(map(lambda x: str(x), array))))


class TestProgram(unittest.TestCase):
    def test_case_1(self):
        output = four_number_sum([1, 2, 3, 4, 5, 6, 7], 10)
        output = list(map(sort_and_stringify, output))
        quadruplets = [[1, 2, 3, 4]]
        self.assertTrue(len(output) == 1)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_2(self):
        output = four_number_sum([7, 6, 4, -1, 1, 2], 16)
        output = list(map(sort_and_stringify, output))
        quadruplets = [[7, 6, 4, -1], [7, 6, 1, 2]]
        self.assertTrue(len(output) == 2)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_3(self):
        output = four_number_sum([5, -5, -2, 2, 3, -3], 0)
        output = list(map(sort_and_stringify, output))
        quadruplets = [[5, -5, -2, 2], [5, -5, 3, -3], [-2, 2, 3, -3]]
        self.assertTrue(len(output) == 3)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_4(self):
        output = four_number_sum([-2, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9], 4)
        output = list(map(sort_and_stringify, output))
        quadruplets = [[-2, -1, 1, 6], [-2, 1, 2, 3], [-2, -1, 2, 5], [-2, -1, 3, 4]]
        self.assertTrue(len(output) == 4)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_5(self):
        output = four_number_sum([-1, 22, 18, 4, 7, 11, 2, -5, -3], 30)
        output = list(map(sort_and_stringify, output))
        quadruplets = [[-1, 22, 7, 2], [22, 4, 7, -3], [-1, 18, 11, 2], [18, 4, 11, -3], [22, 11, 2, -5]]
        self.assertTrue(len(output) == 5)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_6(self):
        output = four_number_sum([-10, -3, -5, 2, 15, -7, 28, -6, 12, 8, 11, 5], 20)
        output = list(map(sort_and_stringify, output))
        quadruplets = [
            [-5, 2, 15, 8],
            [-3, 2, -7, 28],
            [-10, -3, 28, 5],
            [-10, 28, -6, 8],
            [-7, 28, -6, 5],
            [-5, 2, 12, 11],
            [-5, 12, 8, 5],
        ]
        self.assertTrue(len(output) == 7)
        for quadruplet in quadruplets:
            self.assertTrue(sort_and_stringify(quadruplet) in output)

    def test_case_7(self):
        output = four_number_sum([1, 2, 3, 4, 5], 100)
        output = list(map(sort_and_stringify, output))
        self.assertTrue(len(output) == 0)


if __name__ == "__main__":
    unittest.main()

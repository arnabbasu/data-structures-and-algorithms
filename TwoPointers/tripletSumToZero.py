# Given an array of unsorted numbers, find all unique triplets in it that add up to zero
# (note that the array is not sorted)
from typing import List


# sort takes O(nlogn), search_pair is called n times so this function takes O(n^2)
def search_triplets(arr: List[int]) -> List[List[int]]:
    triplets = []
    arr.sort()
    for i, _ in enumerate(arr):
        if i > 0 and arr[i] == arr[i - 1]:  # skip duplicates
            continue
        search_pair(arr, -arr[i], i + 1, triplets)

    return triplets


# this function is O(n)
def search_pair(arr: List[int], target_sum: int, left: int, result: List[List[int]]) -> None:
    if left > len(arr) - 2:
        return
    right = len(arr) - 1
    while left < right:
        if arr[left] + arr[right] == target_sum:
            result.append([-target_sum, arr[left], arr[right]])  # because array is sorted the triplets will be sorted
            left += 1
            right -= 1
            while left < right and arr[left] == arr[left - 1]:  # skip duplicates
                left += 1
            while right > left and arr[right] == arr[right + 1]:  # skip duplicates
                right -= 1
        elif arr[left] + arr[right] < target_sum:
            left += 1
        else:
            right -= 1
    return


def main():
    print(search_triplets([-3, 0, 1, 2, -1, 1, -2]))
    print(search_triplets([-5, 2, -1, -2, 3]))
    print(search_triplets([-5, -5, -5, 2, 2, 2, -1, -2, 3, 3]))


main()

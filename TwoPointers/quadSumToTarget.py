# Given an array of unsorted numbers and a target number, find all unique quadruplets in it, whose sum is equal to the
# target number
from typing import List


# O(n^3)


def search_pair(arr: List[int], start: int, end: int, first: int, second: int, target: int,
                result: List[List[int]]) -> None:
    left, right = start, end
    searching_for = target - first - second
    while left < right:
        current_sum = arr[left] + arr[right]
        if current_sum == searching_for:
            result.append([first, second, arr[left], arr[right]])
            left += 1
            right -= 1
            while left < right and arr[left - 1] == arr[left]:
                left += 1
            while right > left and arr[right + 1] == arr[right]:
                right -= 1
        elif current_sum < searching_for:
            left += 1
        else:
            right -= 1
    return


def search_triplets(arr: List[int], start: int, first: int, target: int, result: List[List[int]]) -> None:
    for i in range(start, len(arr) - 2):
        if i > start and arr[i] == arr[i - 1]:
            continue
        search_pair(arr, i + 1, len(arr) - 1, first, arr[i], target, result)
    return


def search_quadruplets(arr: List[int], target: int) -> List[List[int]]:
    quadruplets = []
    arr.sort()
    for i in range(len(arr) - 3):
        if i > 0 and arr[i] == arr[i - 1]:
            continue
        search_triplets(arr, i + 1, arr[i], target, quadruplets)
    return quadruplets


def main():
    print(search_quadruplets([4, 1, 2, -1, 1, -3], 1))
    print(search_quadruplets([2, 0, -1, 1, -2, 2], 2))
    print(search_quadruplets([20, 0, 20, 10, 20, 20], 2))
    print(search_quadruplets([2, 0, -1], 2))
    print(search_quadruplets([], 2))
    print(search_quadruplets([4, 1, 2, -1, 1, -3], 10))
    print(search_quadruplets([-2, -2, -2, -2, -2, -2, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2], 2))
    print(search_quadruplets([-2, -2, -2, -2, -2, -2, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1], 2))


main()

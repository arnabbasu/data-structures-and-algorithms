# Given an array arr of unsorted numbers and a target sum, count all triplets in it such that
# arr[i] + arr[j] + arr[k] < target where i, j, and k are three different indices. Write a function to return the count
# of such triplets.
from typing import List


# outer loop is O(n), pair_with_smaller_sum is O(n), sort is O(nlogn). Overall O(n^2)


def triplet_with_smaller_sum(arr: List[int], target: int) -> int:
    arr.sort()
    count = 0
    for i in range(len(arr) - 2):
        count += pair_with_smaller_sum(arr, i + 1, target - arr[i])
    return count


def pair_with_smaller_sum(arr: List[int], j: int, target: int) -> int:
    count = 0
    k = len(arr) - 1
    while j < k:
        if arr[j] + arr[k] < target:
            # since arr[k] >= arr[j], therefore, we can replace arr[k] by any number between
            # j and k to get a sum less than the target sum
            count += (k - j)
            j += 1
        else:
            k -= 1
    return count


# Write a function to return the list of all such triplets instead of the count. How will the time complexity change
# in this case?

# O(n^3)


def triplet_with_smaller_sum_list(arr: List[int], target: int) -> List[List[int]]:
    arr.sort()
    result = []
    for i in range(len(arr) - 2):
        pair_with_smaller_sum_list(arr, i + 1, target - arr[i], arr[i], result)
    return result


def pair_with_smaller_sum_list(arr: List[int], j: int, target: int, element: int, result: List[List[int]]) -> None:
    k = len(arr) - 1
    while j < k:
        if arr[j] + arr[k] < target:
            # since arr[k] >= arr[j], therefore, we can replace arr[k] by any number between
            # j and k to get a sum less than the target sum
            for idx in range(j + 1, k + 1):
                result.append([element, arr[j], arr[idx]])
            j += 1
        else:
            k -= 1
    return


def main():
    print(triplet_with_smaller_sum([-1, 0, 2, 3], 3))
    print(triplet_with_smaller_sum([-1, 4, 2, 1, 3], 5))
    print(triplet_with_smaller_sum([6, 2, 4, 5, 8], 17))
    print(triplet_with_smaller_sum([-2, 0, 1, 3], 2))
    print(triplet_with_smaller_sum([5, 1, 4, 3, 7], 12))
    print(triplet_with_smaller_sum([10, 10, 10, 10, 10], 5))
    print(triplet_with_smaller_sum_list([-1, 0, 2, 3], 3))
    print(triplet_with_smaller_sum_list([-1, 4, 2, 1, 3], 5))
    print(triplet_with_smaller_sum_list([6, 2, 4, 5, 8], 17))
    print(triplet_with_smaller_sum_list([-2, 0, 1, 3], 2))
    print(triplet_with_smaller_sum_list([5, 1, 4, 3, 7], 12))
    print(triplet_with_smaller_sum_list([10, 10, 10, 10, 10], 5))


main()

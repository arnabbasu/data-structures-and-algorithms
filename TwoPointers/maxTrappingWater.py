# Suppose you are given an array containing non-negative numbers representing heights of a set of buildings.
# Now, because of differences in heights of buildings water can be trapped between them. Find the two buildings that
# will trap the most amount of water. Write a function that will return the maximum volume of water that will be
# trapped between these two buildings.
from typing import List


def find_max_water(building_heights: List[int]) -> int:
    left, right = 0, len(building_heights) - 1
    max_water, current_water = 0, 0

    # amount of water trapped is limited by shorter building so look for a taller building on that side
    while left < right:
        if building_heights[left] < building_heights[right]:
            current_water = (right - left) * building_heights[left]
            left += 1
        else:
            current_water = (right - left) * building_heights[right]
            right -= 1

        max_water = max(max_water, current_water)
    return max_water


def main():
    print(find_max_water([1, 3, 5, 4, 1]))
    print(find_max_water([3, 2, 5, 4, 2]))
    print(find_max_water([1, 4, 3, 2, 5, 8, 4]))
    print(find_max_water([]))


main()

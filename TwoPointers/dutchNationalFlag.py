# Given an array containing 0s, 1s and 2s, sort the array in-place. You should treat numbers of the array as objects,
# hence, we can’t count 0s, 1s, and 2s to recreate the array.
# Similar:
# Write a program that takes an Array A and an index i into the A and rearranges the elements such that all elements
# less that A[i] appear first, followed by elements equal to A[i], followed by elements greater the A[i]
from typing import List


# O(n)
def dutch_flag_sort(arr: List[int]) -> None:
    # assuming that array can only have 0, 1, 2
    zero_end_index = -1
    next_element_to_check = 0  # one_start_index is next_element_to_check - 1
    two_end_index = len(arr)
    while next_element_to_check < two_end_index:  # handles the empty array case
        if arr[next_element_to_check] == 0:
            zero_end_index += 1
            # zero_end_index is either < or == to next_element_to_check
            # if == then the swap does nothing, if < than we move the last 1 to the beginning
            arr[zero_end_index], arr[next_element_to_check] = arr[next_element_to_check], arr[zero_end_index]
            # in both cases we increment
            next_element_to_check += 1
        elif arr[next_element_to_check] == 2:
            two_end_index -= 1
            arr[next_element_to_check], arr[two_end_index] = arr[two_end_index], arr[next_element_to_check]
            # do not increment next_element_to_check because we could have swapped in either a 0, 1 or 2 to this pos
        else:  # the 1 is already in it's correct place
            next_element_to_check += 1

    return


def main():
    arr = [1, 0, 2, 1, 0]
    dutch_flag_sort(arr)
    print(arr)

    arr = [2, 2, 0, 1, 2, 0]
    dutch_flag_sort(arr)
    print(arr)

    arr = []
    dutch_flag_sort(arr)
    print(arr)

    arr = [0]
    dutch_flag_sort(arr)
    print(arr)

    arr = [1, 1, 1]
    dutch_flag_sort(arr)
    print(arr)

    arr = [2, 2, 2, 2]
    dutch_flag_sort(arr)
    print(arr)


main()

# Given an array of sorted numbers, remove all duplicates from it. You should not use any extra space; after removing
# the duplicates in-place return the new length of the array


def remove_duplicates(arr):
    non_dup_arr_last_elem = 0

    for read_idx in range(len(arr)):
        if arr[non_dup_arr_last_elem] != arr[read_idx]:  # element read does not match last elem of non dup arr
            non_dup_arr_last_elem += 1  # extend the array and copy
            arr[non_dup_arr_last_elem] = arr[read_idx]

    return non_dup_arr_last_elem + 1  # length is one more than index of last element


def main():
    print(remove_duplicates([2, 3, 3, 3, 6, 9, 9]))
    print(remove_duplicates([2, 2, 2, 11]))


main()

# Given a sorted array, create a new array containing squares of all the number of the input array in the sorted order.


# Alternate solution is to start from ends of the array and end of squares, the place the larger square to squares
# and iterate inwards in arr and towards 0 in squares
def make_squares(arr):
    squares = []
    for right_idx, elem in enumerate(arr):  # find the first +ve number in the array
        if elem > 0:
            break

    left_idx = right_idx - 1

    while left_idx >= 0 and right_idx < len(arr):  # move outwards and put the smaller square in the result
        left_sq = arr[left_idx] * arr[left_idx]
        right_sq = arr[right_idx] * arr[right_idx]
        if left_sq < right_sq:
            squares.append(left_sq)
            left_idx -= 1
        else:
            squares.append(right_sq)
            right_idx += 1

    # finish up remaining
    while left_idx >= 0:
        squares.append(arr[left_idx] * arr[left_idx])
        left_idx -= 1

    while right_idx < len(arr):
        squares.append(arr[right_idx] * arr[right_idx])
        right_idx += 1

    return squares


def main():
    print("Squares: " + str(make_squares([-2, -1, 0, 2, 3])))
    print("Squares: " + str(make_squares([-3, -1, 0, 1, 2])))
    print("Squares: " + str(make_squares([-3, -1, 0])))
    print("Squares: " + str(make_squares([-3, -1])))


main()

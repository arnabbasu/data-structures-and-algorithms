# Given an array of sorted numbers and a target sum, find a pair in the array whose sum is equal to the given target


# time O(n), space O(1)
def pair_with_target_sum(arr, target_sum):
    left, right = 0, len(arr) - 1
    while left < right:
        if arr[left] + arr[right] == target_sum:
            return [left, right]
        elif arr[left] + arr[right] < target_sum:  # since the array is sorted, if we are less than sum increase left
            left += 1
        else:  # if we are more than sum increase right
            right -= 1
    return [-1, -1]


# time O(n), space O(n)
# this works in O(n) even if the array is not sorted
def pair_with_target_sum_alt(arr, target_sum):
    seen_numbers = {}  # key is the number in the array, value is it's position in the array
    for i, num in enumerate(arr):
        if target_sum - num in seen_numbers:
            return [seen_numbers[target_sum - num], i]  # this will give correct order because smaller num will be in
            # dict
        else:
            seen_numbers[num] = i
    return [-1, -1]


def main():
    print(pair_with_target_sum([1, 2, 3, 4, 6], 6))
    print(pair_with_target_sum([2, 5, 9, 11], 11))
    print(pair_with_target_sum([1, 2, 3, 4, 6], 11))
    print(pair_with_target_sum_alt([1, 2, 3, 4, 6], 6))
    print(pair_with_target_sum_alt([2, 5, 9, 11], 11))
    print(pair_with_target_sum_alt([1, 2, 3, 4, 6], 11))


main()

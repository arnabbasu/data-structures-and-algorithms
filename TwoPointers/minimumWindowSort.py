# Given an array, find the length of the smallest subarray in it which when sorted will sort the whole array.
from typing import List


def shortest_window_sort_mine(arr: List[int]) -> int:
    if len(arr) == 0:
        return 0

    # start from the right of the array and find the index of the last swap required to "bubble" the smallest element
    # to the left
    last_left_swap_index = len(arr) - 1
    current_element = arr[-1]
    for i in range(len(arr) - 1, 0, -1):
        if current_element < arr[i - 1]:
            last_left_swap_index = i - 1
        else:
            current_element = arr[i - 1]

    # start from the left of the array and find the index of the last swap required to "bubble" the largest element
    # to the right
    last_right_swap_index = 0
    current_element = arr[0]
    for i in range(len(arr) - 1):
        if current_element > arr[i + 1]:
            last_right_swap_index = i + 1
        else:
            current_element = arr[i + 1]

    return last_right_swap_index - last_left_swap_index + 1 if last_right_swap_index > last_left_swap_index else 0


def shortest_window_sort_theirs(arr: List[int]) -> int:
    if len(arr) == 0:
        return 0
    # from the left find the first number out of sorted order
    left = 0
    while left < len(arr) - 1 and arr[left] <= arr[left + 1]:
        left += 1

    if left == len(arr) - 1:  # sorted
        return 0

    # from the right find the last (first from right) number out of sorted order
    right = len(arr) - 1
    while right > 0 and arr[right] >= arr[right - 1]:
        right -= 1

    # find min and max of the sub array
    min_of_array = min(arr[left:right + 1])
    max_of_array = max(arr[left:right + 1])

    # anything outside the sub array on the left sorted part that is smaller than min must be included
    while left > 0 and arr[left - 1] > min_of_array:
        left -= 1
    # anything outside the sub array on the right sorted part that is larger than the max must be included
    while right < len(arr) - 1 and arr[right + 1] < max_of_array:
        right += 1

    return right - left + 1


def main():
    print(shortest_window_sort_mine([1, 2, 5, 3, 7, 10, 9, 12]))
    print(shortest_window_sort_mine([1, 3, 2, 0, -1, 7, 10]))
    print(shortest_window_sort_mine([1, 2, 3]))
    print(shortest_window_sort_mine([3, 2, 1]))
    print(shortest_window_sort_mine([]))
    print(shortest_window_sort_mine([1]))
    print(shortest_window_sort_mine([-1, 0, 10, 1, 2, 3, 7]))
    print("=======")
    print(shortest_window_sort_theirs([1, 2, 5, 3, 7, 10, 9, 12]))
    print(shortest_window_sort_theirs([1, 3, 2, 0, -1, 7, 10]))
    print(shortest_window_sort_theirs([1, 2, 3]))
    print(shortest_window_sort_theirs([3, 2, 1]))
    print(shortest_window_sort_theirs([]))
    print(shortest_window_sort_theirs([1]))
    print(shortest_window_sort_theirs([-1, 0, 10, 1, 2, 3, 7]))


main()

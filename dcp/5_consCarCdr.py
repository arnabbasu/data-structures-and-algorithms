# cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair.
# (closures)


def cons(a, b):
    def pair(f):
        return f(a, b)

    return pair


def car(pair):
    def first(a, _):
        return a

    return pair(first)


def cdr(pair):
    def second(_, b):
        return b

    return pair(second)


if __name__ == '__main__':
    print(car(cons(3, 4)))
    print(cdr(cons(3, 4)))

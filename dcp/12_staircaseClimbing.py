# There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time. Given N, write a function
# that returns the number of unique ways you can climb the staircase. The order of the steps matters.
#
# For example, if N is 4, then there are 5 unique ways:
#
# 1, 1, 1, 1
# 2, 1, 1
# 1, 2, 1
# 1, 1, 2
# 2, 2
# What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive
# integers X? For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time.
from typing import Set


# Approach 1: Recursive


def count_ways_to_climb_rec(target_floor: int, cur_floor: int = 0, num_ways: int = 0) -> int:
    if cur_floor + 1 == target_floor:
        num_ways += 1
    elif cur_floor + 1 < target_floor:
        num_ways = count_ways_to_climb_rec(target_floor, cur_floor + 1, num_ways)

    if cur_floor + 2 == target_floor:
        num_ways += 1
    elif cur_floor + 2 < target_floor:
        num_ways = count_ways_to_climb_rec(target_floor, cur_floor + 2, num_ways)

    return num_ways


# Approach 2: Non recursive
# Insight: All ways of reaching N -1 will contribute once, because we climb one step from those
#          All ways of reaching N - 2 will contribute once because we climb two steps from those


def count_ways_to_climb_loop(target_floor: int) -> int:
    if target_floor == 0:
        return 0
    if target_floor == 1:
        return 1

    num_ways_prev_prev = 1
    num_ways_prev = 1

    for tf in range(2, target_floor + 1):
        num_ways_prev_prev, num_ways_prev = num_ways_prev, num_ways_prev_prev + num_ways_prev

    return num_ways_prev


# Approach 3: Dynamic programming (O(N * |X|) time and O(N) space)
# In addition to the above insight we need to handle the scenario in which the target goes above another way to climb
# eg, in the set {1, 3, 5} we can use 1 while N = 1 and 2, we can use 1 and 3 when N >= 3 and we can use 1, 3, 5 when
# N >= 5


def count_ways_to_climb_ext(target_floor: int, ways_to_climb: Set) -> int:
    if len(ways_to_climb) == 0:
        return 0

    if target_floor < min(ways_to_climb):  # change from the above where we were returining 0
        return 1

    num_ways = [0 for _ in range(target_floor + 1)]

    for tf in range(target_floor + 1):
        num_ways[tf] = sum(num_ways[tf - e] for e in ways_to_climb if tf - e > 0)
        num_ways[tf] += 1 if tf in ways_to_climb else 0

    return num_ways[-1]


# model solution


def staircase(n, X):
    if n < 0:
        return 0
    elif n == 0:
        return 1
    elif n in X:
        return 1 + sum(staircase(n - x, X) for x in X if x < n)
    else:
        return sum(staircase(n - x, X) for x in X if x < n)


if __name__ == '__main__':
    print("=== recursive ===")
    print(count_ways_to_climb_rec(0))
    print(count_ways_to_climb_rec(1))
    print(count_ways_to_climb_rec(2))
    print(count_ways_to_climb_rec(3))
    print(count_ways_to_climb_rec(4))
    print(count_ways_to_climb_rec(5))
    print(count_ways_to_climb_rec(6))
    print("=== loop ===")
    print(count_ways_to_climb_loop(0))
    print(count_ways_to_climb_loop(1))
    print(count_ways_to_climb_loop(2))
    print(count_ways_to_climb_loop(3))
    print(count_ways_to_climb_loop(4))
    print(count_ways_to_climb_loop(5))
    print(count_ways_to_climb_loop(6))
    print("=== Generic ===")
    print(count_ways_to_climb_ext(0, {1, 2}))
    print(count_ways_to_climb_ext(1, {1, 2}))
    print(count_ways_to_climb_ext(2, {1, 2}))
    print(count_ways_to_climb_ext(3, {1, 2}))
    print(count_ways_to_climb_ext(4, {1, 2}))
    print(count_ways_to_climb_ext(5, {1, 2}))
    print(count_ways_to_climb_ext(6, {1, 2}))
    print("=== Generic {1, 3, 5}===")
    print(count_ways_to_climb_ext(0, {1, 3, 5}))
    print(count_ways_to_climb_ext(1, {1, 3, 5}))
    print(count_ways_to_climb_ext(2, {1, 3, 5}))
    print(count_ways_to_climb_ext(3, {1, 3, 5}))
    print(count_ways_to_climb_ext(4, {1, 3, 5}))
    print(count_ways_to_climb_ext(5, {1, 3, 5}))
    print(count_ways_to_climb_ext(6, {1, 3, 5}))
    print("=== test ===")
    print(staircase(0, {1, 3, 5}))
    print(staircase(1, {1, 3, 5}))
    print(staircase(2, {1, 3, 5}))
    print(staircase(3, {1, 3, 5}))
    print(staircase(4, {1, 3, 5}))
    print(staircase(5, {1, 3, 5}))
    print(staircase(6, {1, 3, 5}))
    print("=== test cases ===")
    print(count_ways_to_climb_ext(0, {1, 3, 5}) == staircase(0, {1, 3, 5}))
    print(count_ways_to_climb_ext(1, {1, 3, 5}) == staircase(1, {1, 3, 5}))
    print(count_ways_to_climb_ext(2, {1, 3, 5}) == staircase(2, {1, 3, 5}))
    print(count_ways_to_climb_ext(3, {1, 3, 5}) == staircase(3, {1, 3, 5}))
    print(count_ways_to_climb_ext(4, {1, 3, 5}) == staircase(4, {1, 3, 5}))
    print(count_ways_to_climb_ext(5, {1, 3, 5}) == staircase(5, {1, 3, 5}))
    print(count_ways_to_climb_ext(6, {1, 3, 5}) == staircase(6, {1, 3, 5}))

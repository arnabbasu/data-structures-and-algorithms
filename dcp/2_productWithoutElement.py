# Given an array of integers, return a new array such that each element at index i of the new array is the product of
# all the numbers in the original array except the one at i, don't use division
from typing import List


def product_without_element(arr: List[int]) -> List[int]:
    # if the input array has fewer than two integers there won't be any products to return because at any index there
    # are no “other” integers
    if len(arr) < 2:
        return []
    result = [0 for x in range(len(arr))]

    # result holds array of product of 0 *..* i-1, first element is 1 (identity element) because there is nothing
    # before arr[0] we are not interest in the product of all elements up to last element because it is never used
    cur_forward_product = 1
    for idx in range(len(arr)):
        result[idx] = cur_forward_product
        cur_forward_product *= arr[idx]

    # for each integer, we find the product of all the integers after it. since each index in results already has the
    # product of all the integers before it, now we're storing the total product of all other integers
    curr_reverse_product = 1
    for idx in reversed(range(len(arr))):
        result[idx] = curr_reverse_product * result[idx]
        curr_reverse_product *= arr[idx]
    return result


if __name__ == '__main__':
    print(product_without_element([1, 2, 3, 4, 5]))
    print(product_without_element([0, 0, 0, 0, 0]))
    print(product_without_element([]))
    print(product_without_element([1]))
    print(product_without_element([1, 1, 1, 1, 1, 1]))
    print(product_without_element([10, 20]))

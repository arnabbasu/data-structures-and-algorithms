# Given an array of integers, find the first missing positive integer in linear time and constant space. In other
# words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and
# negative numbers as well. You can modify the input array in-place
from typing import List


def smallest_missing_positive_int(arr: List[int]) -> int:
    if len(arr) == 0:
        return 1
    if len(arr) == 1 and arr[0] != 1:
        return 1

    range_max = len(arr)
    # 1st pass, we look for elements between 1 and array length and put them at their correct index, i.e value - 1
    for i in range(range_max):
        # continue swapping if element is not at it's right position and it is within range and if swapping would not
        # just bring a duplicate element to this position
        while arr[i] != i + 1 and (1 <= arr[i] <= range_max) and arr[arr[i] - 1] != arr[i]:
            t = arr[arr[i] - 1]
            arr[arr[i] - 1] = arr[i]
            arr[i] = t

    # ideally all positions in the array should hold the number position + 1
    for i, e in enumerate(arr):
        if e != i + 1:
            return i + 1

    return range_max + 1


if __name__ == '__main__':
    print(smallest_missing_positive_int([3, 4, -1, 1]) == 2)
    print(smallest_missing_positive_int([1]) == 2)
    print(smallest_missing_positive_int([2]) == 1)
    print(smallest_missing_positive_int([]) == 1)
    print(smallest_missing_positive_int([1, 2, 0]) == 3)
    print(smallest_missing_positive_int([-1, -1, 0]) == 1)
    print(smallest_missing_positive_int([-1, -1, -1]) == 1)
    print(smallest_missing_positive_int([1, 1, 1]) == 2)
    print(smallest_missing_positive_int([2, 2, 2]) == 1)
    print(smallest_missing_positive_int([10, 10, 10]) == 1)
    print(smallest_missing_positive_int([3, 2, 1]) == 4)
    print(smallest_missing_positive_int([-10, -3, -100, -1000, -239, 1]) == 2)

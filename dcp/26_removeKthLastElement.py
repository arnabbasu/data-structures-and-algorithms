# Given a singly linked list and an integer k, remove the kth last element from the list. k is guaranteed to be smaller
# than the length of the list. Do it in one pass


class ListNode:
    def __init__(self: 'ListNode', x: int):
        self.val = x
        self.next = None

    def print_list(self: 'ListNode') -> None:
        head = self
        while head is not None:
            print(head.val, end='')
            head = head.next
        print()


class Solution:
    @staticmethod
    def remove_nth_from_end(head: ListNode, n: int) -> ListNode:
        fast = head
        for i in range(n):
            fast = fast.next

        if fast is None:  # remove head
            head = head.next
            return head

        if fast.next is None:  # remove 1st node
            head.next = head.next.next
            return head

        slow, fast = head, fast.next
        while fast is not None:
            slow = slow.next
            fast = fast.next

        slow.next = slow.next.next
        return head


def main():
    head = ListNode(1)
    head.next = ListNode(2)
    head.next.next = ListNode(3)
    head.next.next.next = ListNode(4)
    head.next.next.next.next = ListNode(5)

    head = Solution.remove_nth_from_end(head, 5)
    head.print_list()
    h = ListNode(1)
    h.next = head
    head = h
    head = Solution.remove_nth_from_end(head, 4)
    head.print_list()
    h = ListNode(2)
    h.next = head.next
    head.next = h
    head = Solution.remove_nth_from_end(head, 2)
    head.print_list()


main()

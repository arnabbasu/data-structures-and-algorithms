# Given an array of integers and a number k, where 1 <= k <= length of the array, compute the maximum values of each
# subarray of length k.
from typing import List
from collections import deque

class StackWithMax:
    def __init__(self):
        self.__stack = []
        self.__max_stack = []

    def Push(self, a):
        self.__stack.append(a)
        if len(self.__max_stack) == 0 or a >= self.__max_stack[-1]:
            self.__max_stack.append(a)

    def Pop(self):
        assert(len(self.__stack))
        if self.__max_stack[-1] == self.__stack[-1]:
            self.__max_stack.pop()
        return self.__stack.pop()

    def Max(self):
        assert(len(self.__stack))
        return self.__max_stack[-1]

    def __len__(self):
        return len(self.__stack)


class Queue:
    """
        queue with two stacks
    """
    def __init__(self):
        self.__inbox = StackWithMax()
        self.__outbox = StackWithMax()

    def Enqueue(self, a):
        self.__inbox.Push(a)

    def Dequeue(self):
        assert (len(self.__outbox) > 0 or len(self.__inbox) > 0)
        if len(self.__outbox) == 0:
            while len(self.__inbox) > 0:
                self.__outbox.Push(self.__inbox.Pop())
        return self.__outbox.Pop()

    def Max(self):
        assert (len(self.__outbox) > 0 or len(self.__inbox) > 0)
        m = -float('inf')
        if len(self.__outbox) > 0:
            m = max(m, self.__outbox.Max())
        if len(self.__inbox) > 0:
            m = max(m, self.__inbox.Max())
        return m


def max_sliding_window(sequence, m):
    maximums = []
    q = Queue()
    for i in range(m):
        q.Enqueue(sequence[i])

    maximums.append(q.Max())

    for i in range(m, len(sequence)):
        q.Dequeue()
        q.Enqueue(sequence[i])
        maximums.append(q.Max())

    return maximums


def max_in_each_subarray(arr: List[int], k: int) -> List[int]:
    if k == 0 or k > len(arr):
        return []
    # possible_max holds the indexes of all the candidates for max in a sub array. Every time we slide the window
    # we need to
    # 1. drop any index that has gone out of range
    # 2. drop all elements that are smaller than the incoming element, this is because while the incoming element is
    # in the window, no smaller element earlier in the window can be the max, so we can discard them
    possible_max = deque(maxlen=k)  # holds INDEXES of 'useful' elements in sorted order
    results = []

    # initially we grow the window, at this stage we do not need to care about index based expiry
    for right_index in range(k):
        # while the deque is not empty and contains indexes of elements smaller than current
        while len(possible_max) != 0 and arr[possible_max[-1]] < arr[right_index]:
            possible_max.pop()
        possible_max.append(right_index)

    results.append(arr[possible_max[0]])
    left_index = 1
    # now we start sliding the window
    for right_index in range(k, len(arr)):
        while len(possible_max) != 0 and possible_max[0] < left_index:
            possible_max.popleft()

        while len(possible_max) != 0 and arr[possible_max[-1]] < arr[right_index]:
            possible_max.pop()
        possible_max.append(right_index)
        results.append(arr[possible_max[0]])
        left_index += 1

    return results


if __name__ == '__main__':
    print(max_in_each_subarray([10, 5, 2, 7, 8, 7], 3))
    print(max_in_each_subarray([], 1))
    print(max_in_each_subarray([10, 5, 2, 7, 8, 7], 6))
    print(max_in_each_subarray([1, 3, -1, -3, 5, 3, 6, 7], 3))

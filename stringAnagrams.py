# Given a string and a pattern, find all anagrams of the pattern in the given string.


def string_anagrams(st, pat):
    window_start, start_indexes = 0, []
    chars_in_pat = {}
    matched = 0

    # build dict which holds freq of chars in pattern
    for c in pat:
        if c in chars_in_pat:
            chars_in_pat[c] += 1
        else:
            chars_in_pat[c] = 1

    # expand the window
    for window_end in range(len(st)):
        cur_char = st[window_end]
        # if character exists in pattern account for it
        if cur_char in chars_in_pat:
            chars_in_pat[cur_char] -= 1
            if chars_in_pat[cur_char] == 0:
                matched += 1
        # if all characters in pattern are found in current window record the start index
        if matched == len(chars_in_pat):
            start_indexes.append(window_start)

        # shrink the window if it is longer or equal to the pattern length
        if (window_end - window_start + 1) >= len(pat):
            left_char = st[window_start]
            window_start += 1
            # handle the case where outgoing character is part of the pattern
            if left_char in chars_in_pat:
                if chars_in_pat[left_char] == 0:
                    matched -= 1
                chars_in_pat[left_char] += 1

    return start_indexes


def main():
    print(string_anagrams("ppqp", "pq"))
    print(string_anagrams("abbcabc", "abc"))


main()

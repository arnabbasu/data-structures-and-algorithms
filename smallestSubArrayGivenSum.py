# Given an array of positive numbers and a positive number ‘S’, find the length of the smallest subarray whose sum is
# greater than or equal to ‘S’. Return 0, if no such subarray exists


def smallest_subarray_given_sum(s, arr):
    infinity = len(arr) + 1  # represent infinity with an impossible subarray length
    current_sum, window_start, min_length = 0, 0, infinity
    for window_end in range(len(arr)):
        current_sum += arr[window_end]  # add the current number to the sum
        while current_sum >= s:
            min_length = min(min_length, window_end - window_start + 1)  # update the min length if curr length is smaller
            current_sum -= arr[window_start]  # shrink the subarray and reduce the sum
            window_start += 1

    if min_length == infinity:
        return 0
    else:
        return min_length


def main():
    print("Smallest subarray length: " + str(smallest_subarray_given_sum(7, [2, 1, 5, 2, 3, 2])))
    print("Smallest subarray length: " + str(smallest_subarray_given_sum(7, [2, 1, 5, 2, 8])))
    print("Smallest subarray length: " + str(smallest_subarray_given_sum(8, [3, 4, 1, 1, 6])))


main()

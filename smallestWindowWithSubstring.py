# Given a string and a pattern, find the smallest substring in the given string which has all the characters of the
# given pattern


def smallest_window_with_ss(st, pat):
    window_start, res = 0, ""
    chars_in_pat, matched = {}, 0

    # create dictionary of characters in pattern
    for c in pat:
        if c in chars_in_pat:
            chars_in_pat[c] += 1
        else:
            chars_in_pat[c] = 1

    # expand window
    for window_end in range(len(st)):
        cur_char = st[window_end]
        # handle character from pattern
        if cur_char in chars_in_pat:
            chars_in_pat[cur_char] -= 1
            if chars_in_pat[cur_char] == 0: # if num occurrences match those in pattern record
                matched += 1

        # if we have matched all chars of pattern shorten the window until we stop finding a character of the pattern
        while matched == len(chars_in_pat) and window_start < window_end:
            # if 1st match or shorter match save the result
            if len(res) == 0 or (window_end - window_start + 1) < len(res):
                res = st[window_start:window_end+1]
            left_char = st[window_start]
            window_start += 1
            if left_char in chars_in_pat:
                # we are about to drop a character of pattern that we need
                if chars_in_pat[left_char] == 0:
                    matched -= 1
                chars_in_pat[left_char] += 1

    return res


def main():
    print(smallest_window_with_ss("aabdec", "abc"))
    print(smallest_window_with_ss("abdabca", "abc"))
    print(smallest_window_with_ss("adcad", "abc"))


main()

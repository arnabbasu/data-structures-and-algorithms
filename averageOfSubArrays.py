# Given an array, find the average of all subarrays of size ‘K’ in it.
def find_averages_of_subarrays(sub_arr_len, arr):
    if sub_arr_len > len(arr):
        raise Exception(
            'subarray len should not exceed the length of the array. subarray len is {}, array length is {}'.format(
                sub_arr_len, len(arr)))
    result = []
    window_sum, window_start = 0.0, 0

    # expand window from left to right until it reaches sub_arr_len
    for window_end in range(sub_arr_len):
        window_sum += arr[window_end]
    result.append(window_sum / sub_arr_len)  # calculate the 1st average and append to result

    # for the remaining elements of the array
    for window_end in range(sub_arr_len, len(arr)):
        window_sum -= arr[window_start]  # subtract outgoing element from sum
        window_sum += arr[window_end]  # add incoming element to sum
        result.append(window_sum / sub_arr_len)
        window_start += 1  # slide the window

    return result


def main():
    result = find_averages_of_subarrays(5, [1, 3, 2, 6, -1, 4, 1, 8, 2])
    print("Averages of subarrays of size K: " + str(result))


main()

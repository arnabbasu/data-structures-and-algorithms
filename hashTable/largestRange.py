# Write a function that takes in an array of integers and returns an array of length 2 representing the largest range
# of numbers contained in that array. The first number in the output array should be the first number in the range
# while the second number should be the last number in the range. A range of numbers is defined as a set of numbers
# that come right after each other in the set of real integers.
#
# For instance, the output array [2, 6] represents the range {2, 3, 4, 5, 6}, which is a range of length 5. Note that
# numbers do not need to be ordered or adjacent in the array in order to form a range. Assume that there will only be
# one largest range.
#
# Your algorithm should run in O(n) complexity


import unittest


# O(nlogn) time | O(1) space
def largest_range1(array):
    array.sort()
    start = 0
    end = 0
    result = [array[0], array[0]]
    while end < len(array) - 1:
        while end < len(array) - 1 and (array[end + 1] == array[end] or array[end + 1] == array[end] + 1):
            end = end + 1
        if (array[end] - array[start]) + 1 > (result[1] - result[0]) + 1:
            result = [array[start], array[end]]
        start = end + 1
        end = start

    return result


# O(n) time | O(n) space
def largest_range(array):
    present = {}
    for num in array:
        present[num] = True

    longest_range = 0
    result = []
    for num in array:
        if not present[num]:
            continue

        present[num] = False
        range_start = num
        while range_start - 1 in present:
            present[range_start - 1] = False
            range_start = range_start - 1

        range_end = num
        while range_end + 1 in present:
            present[range_end + 1] = False
            range_end = range_end + 1

        if (range_end - range_start) + 1 > longest_range:
            longest_range = (range_end - range_start) + 1
            result = [range_start, range_end]

    return result


class TestProgram(unittest.TestCase):
    def test_case_1(self):
        self.assertEqual(largest_range([1]), [1, 1])

    def test_case_2(self):
        self.assertEqual(largest_range([1, 2]), [1, 2])

    def test_case_3(self):
        self.assertEqual(largest_range([4, 2, 1, 3]), [1, 4])

    def test_case_4(self):
        self.assertEqual(largest_range([4, 2, 1, 3, 6]), [1, 4])

    def test_case_5(self):
        self.assertEqual(largest_range([8, 4, 2, 10, 3, 6, 7, 9, 1]), [6, 10])

    def test_case_6(self):
        self.assertEqual(largest_range([1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6]), [0, 7])

    def test_case_7(self):
        self.assertEqual(
            largest_range([19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14]),
            [10, 19],
        )

    def test_case_8(self):
        self.assertEqual(
            largest_range(
                [0, 9, 19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14]
            ),
            [-1, 19],
        )

    def test_case_9(self):
        self.assertEqual(
            largest_range(
                [
                    0,
                    -5,
                    9,
                    19,
                    -1,
                    18,
                    17,
                    2,
                    -4,
                    -3,
                    10,
                    3,
                    12,
                    5,
                    16,
                    4,
                    11,
                    7,
                    -6,
                    -7,
                    6,
                    15,
                    12,
                    12,
                    2,
                    1,
                    6,
                    13,
                    14,
                    -2,
                ]
            ),
            [-7, 7],
        )

    def test_case_10(self):
        self.assertEqual(
            largest_range(
                [
                    0,
                    -5,
                    9,
                    19,
                    -1,
                    18,
                    17,
                    2,
                    -4,
                    -3,
                    10,
                    3,
                    12,
                    5,
                    16,
                    4,
                    11,
                    7,
                    -6,
                    -7,
                    6,
                    15,
                    12,
                    12,
                    2,
                    1,
                    6,
                    13,
                    14,
                    -2,
                ]
            ),
            [-7, 7],
        )

    def test_case_11(self):
        self.assertEqual(
            largest_range(
                [
                    -7,
                    -7,
                    -7,
                    -7,
                    8,
                    -8,
                    0,
                    9,
                    19,
                    -1,
                    -3,
                    18,
                    17,
                    2,
                    10,
                    3,
                    12,
                    5,
                    16,
                    4,
                    11,
                    -6,
                    8,
                    7,
                    6,
                    15,
                    12,
                    12,
                    -5,
                    2,
                    1,
                    6,
                    13,
                    14,
                    -4,
                    -2,
                ]
            ),
            [-8, 19],
        )


if __name__ == "__main__":
    unittest.main()

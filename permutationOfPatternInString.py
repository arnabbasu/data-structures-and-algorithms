# Given a string and a pattern, find out if the string contains any permutation of the pattern.


def find_permutation(st, pat):
    window_start, matched = 0, 0
    # pattern_map will contain +ve numbers if the substring contains less than the required number of occurrences
    # of that particular character and -ve numbers if the substring contains more than the required number of
    # occurrences of that character
    pattern_map = {}

    # build a map which records the occurrence and freq of each char in pattern
    for char in pat:
        if char in pattern_map:
            pattern_map[char] += 1
        else:
            pattern_map[char] = 1

    # expand the window
    for window_end in range(len(st)):
        cur_char = st[window_end]
        # if current character is in the pattern, record its occurrence, if there is more than the required number
        # of this character present in the substring we allow the map count to go -ve
        if cur_char in pattern_map:
            pattern_map[cur_char] -= 1  # intentionally allowing count to go -ve
            if pattern_map[cur_char] == 0:  # we only update matched on a zero crossing
                matched += 1

        if matched == len(pattern_map):  # all unique characters in the pattern have been found
            return True

        # window length is longer than pattern, shrink from the left and deal with the departing character
        # (equality here because the window will expand above)
        if (window_end - window_start + 1) >= len(pat):
            left_char = st[window_start]
            window_start += 1
            if left_char in pattern_map:  # departing character is a pattern character
                # count of the departing character was neither +ve nor -ve so it was found just the right number of
                # times, dropping this character will cause the match count to be updated
                if pattern_map[left_char] == 0:
                    matched -= 1
                pattern_map[left_char] += 1  # note, this must be done after the zero check

    return False


def main():
    print('Permutation exist: ' + str(find_permutation("oidbcaf", "abc")))
    print('Permutation exist: ' + str(find_permutation("odicf", "dc")))
    print('Permutation exist: ' + str(find_permutation("bcdxabcdy", "bcdyabcdx")))
    print('Permutation exist: ' + str(find_permutation("aaacb", "abc")))


main()

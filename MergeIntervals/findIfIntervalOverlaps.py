# Given a set of intervals, find out if any two intervals overlap
from typing import List


class Interval:
    def __init__(self: 'Interval', start: int, end: int):
        self.start = start
        self.end = end

    def __repr__(self: 'Interval') -> str:
        return "[" + str(self.start) + ", " + str(self.end) + "]"

    def __lt__(self: 'Interval', other: 'Interval') -> bool:
        return self.start < other.start


def has_overlapping_interval(intervals: List['Interval']) -> bool:
    if len(intervals) < 2:
        return False
    intervals.sort()
    for i in range(1, len(intervals)):
        if intervals[i].start < intervals[i - 1].end:
            return True

    return False


def main():
    intervals = [Interval(1, 4), Interval(2, 5), Interval(7, 9)]
    for i in intervals:
        print(i, end='')
    print(" has overlap = " + str(has_overlapping_interval(intervals)))
    intervals = [Interval(1, 2), Interval(3, 4), Interval(5, 6)]
    for i in intervals:
        print(i, end='')
    print(" has overlap = " + str(has_overlapping_interval(intervals)))


main()

# Given a list of non-overlapping intervals sorted by their start time, insert a given interval at the correct position
# and merge all necessary intervals to produce a list that has only mutually exclusive intervals.
from typing import List

START = 0
END = 1


def merge(interval1: List[int], interval2: List[int]) -> bool:
    if interval2[START] > interval1[END]:
        return False
    if interval2[END] > interval1[END]:
        interval1[END] = interval2[END]
    return True


def insert(intervals: List[List[int]], new_interval: List[int]) -> List[List[int]]:
    merged = []
    inserted = False
    for i in range(len(intervals)):
        current_interval = intervals[i]
        if not inserted and current_interval[START] > new_interval[START]:  # insert new_interval and merge with next
            merged.append(new_interval)
            if not merge(merged[-1], current_interval):
                merged.append(current_interval)
            inserted = True
        elif inserted:  # new_interval has been inserted, see if remaining intervals can be merged
            if not merge(merged[-1], current_interval):
                merged.append(current_interval)
        else:  # current_interval is before new_interval in the list
            merged.append(current_interval)
    return merged


def insert1(intervals: List[List[int]], new_interval: List[int]) -> List[List[int]]:
    merged = []
    current_index = 0

    while current_index < len(intervals) and intervals[current_index][END] < new_interval[START]:
        merged.append(intervals[current_index])
        current_index += 1

    # the interval at current_index has end time >= new_interval start,
    # now either:
    # 1. its start time is before, same as new_interval, i.e. overlap or
    # 2. its start time is after new_interval end (no overlap)
    # this process continues until there are no overlaps
    while current_index < len(intervals) and intervals[current_index][START] <= new_interval[END]:
        new_interval[START] = min(intervals[current_index][START], new_interval[START])
        new_interval[END] = max(intervals[current_index][END], new_interval[END])
        current_index += 1

    merged.append(new_interval)

    # append remaining
    while current_index < len(intervals):
        merged.append(intervals[current_index])
        current_index += 1

    return merged


def main():
    print("Intervals after inserting the new interval: " + str(insert([[1, 3], [5, 7], [8, 12]], [4, 6])))
    print("Intervals after inserting the new interval: " + str(insert([[1, 3], [5, 7], [8, 12]], [4, 10])))
    print("Intervals after inserting the new interval: " + str(insert([[2, 3], [5, 7]], [1, 4])))
    print("=====")
    print("Intervals after inserting the new interval: " + str(insert1([[1, 3], [5, 7], [8, 12]], [4, 6])))
    print("Intervals after inserting the new interval: " + str(insert1([[1, 3], [5, 7], [8, 12]], [4, 10])))
    print("Intervals after inserting the new interval: " + str(insert1([[2, 3], [5, 7]], [1, 4])))

main()

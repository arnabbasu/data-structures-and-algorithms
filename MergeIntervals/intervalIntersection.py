# Given two lists of intervals, find the intersection of these two lists. Each list consists of disjoint intervals sorted on their start time.
import unittest
from typing import List
import itertools

START, END = 0, 1

def merge(intervals_a: List[List[int]], intervals_b: List[List[int]]) -> List[List[int]]:
    results = []
    if len(intervals_a) == 0 or len(intervals_b) == 0:
        return results

    a, b = 0, 0
    while a < len(intervals_a) and b < len(intervals_b):
        r = get_intersection(intervals_a[a], intervals_b[b])
        end_a, end_b = intervals_a[a][END], intervals_b[b][END]
        if len(r) != 0:
            results.append(r)
        if end_a <= end_b:
            a = a + 1
        if end_b <= end_a:
            b = b + 1
    return results


def get_intersection(first: List[int], second: List[int]) -> List[int]:
    if first is None or second is None:
        return []

    if first[START] > second[START]:
        second, first = first, second

    if second[START] > first[END]:
        return []

    end = min(first[END], second[END])
    return [second[START], end]


class TestSolution(unittest.TestCase):
    def test_one(self):
        self.assertEqual(get_intersection([1, 3], [2, 3]), [2, 3])
        self.assertEqual(get_intersection([1, 2], [3, 4]), [])
        self.assertEqual(get_intersection([1, 4], [2, 5]), [2, 4])
        self.assertEqual(get_intersection([1, 4], [2, 3]), [2, 3])
        self.assertEqual(get_intersection([2, 3], [1, 3]), [2, 3])
        self.assertEqual(get_intersection([3, 4], [1, 2]), [])
        self.assertEqual(get_intersection([2, 5], [1, 4]), [2, 4])
        self.assertEqual(get_intersection([2, 3], [1, 4]), [2, 3])
        self.assertEqual(get_intersection([5, 7], [7, 9]), [7,7])

    def test_two(self):
        self.assertEqual(merge([[1, 3], [5, 6], [7, 9]], [[2, 3], [5, 7]]), [[2, 3], [5, 6], [7, 7]])
        self.assertEqual(merge([[1, 3], [5, 7], [9, 12]], [[5, 10]]), [[5, 7], [9, 10]])
        self.assertEqual(merge([[1, 3], [5, 7]], [[1, 3], [5, 7]]), [[1, 3], [5, 7]])

if __name__ == '__main__':
    unittest.main()

# Given a list of intervals representing the start and end time of ‘N’ meetings, find the minimum number of rooms
# required to hold all the meetings

from heapq import *


class Meeting:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __lt__(self, other):
        return self.end < other.end


def min_meeting_rooms(meetings):
    schedule = {}
    for meeting in meetings:
        for hour in range(meeting.start, meeting.end):
            if hour in schedule:
                schedule[hour] = schedule[hour] + 1
            else:
                schedule[hour] = 1

    return max(schedule.values())


def min_meeting_rooms_1(meetings):
    meetings.sort(key=lambda x: x.start)

    min_rooms = 0
    min_heap = []  # the min heap will hold all currently active meetings
    for meeting in meetings:
        # remove all meetings ending before the current one starts
        while len(min_heap) > 0 and meeting.start >= min_heap[0].end:
            heappop(min_heap)

        # add current meeting to the list
        heappush(min_heap, meeting)
        # get max number of meetings that have run at once
        min_rooms = max(min_rooms, len(min_heap))
    return min_rooms


def main():
    print("Minimum meeting rooms required: " + str(min_meeting_rooms(
        [Meeting(4, 5), Meeting(2, 3), Meeting(2, 4), Meeting(3, 5)])))
    print("Minimum meeting rooms required 1: " + str(min_meeting_rooms_1(
        [Meeting(4, 5), Meeting(2, 3), Meeting(2, 4), Meeting(3, 5)])))
    print("Minimum meeting rooms required: " +
          str(min_meeting_rooms([Meeting(1, 4), Meeting(2, 5), Meeting(7, 9)])))
    print("Minimum meeting rooms required 1: " +
          str(min_meeting_rooms_1([Meeting(1, 4), Meeting(2, 5), Meeting(7, 9)])))
    print("Minimum meeting rooms required: " +
          str(min_meeting_rooms([Meeting(6, 7), Meeting(2, 4), Meeting(8, 12)])))
    print("Minimum meeting rooms required 1: " +
          str(min_meeting_rooms_1([Meeting(6, 7), Meeting(2, 4), Meeting(8, 12)])))
    print("Minimum meeting rooms required: " +
          str(min_meeting_rooms([Meeting(1, 4), Meeting(2, 3), Meeting(3, 6)])))
    print("Minimum meeting rooms required 1: " +
          str(min_meeting_rooms_1([Meeting(1, 4), Meeting(2, 3), Meeting(3, 6)])))
    print("Minimum meeting rooms required: " + str(min_meeting_rooms(
        [Meeting(4, 5), Meeting(2, 3), Meeting(2, 4), Meeting(3, 5)])))
    print("Minimum meeting rooms required 1: " + str(min_meeting_rooms_1(
        [Meeting(4, 5), Meeting(2, 3), Meeting(2, 4), Meeting(3, 5)])))


main()

# Given a list of intervals, merge all the overlapping intervals to produce a list that has only mutually exclusive
# intervals
from typing import List


class Interval:
    def __init__(self: 'Interval', start: int, end: int):
        self.start = start
        self.end = end

    def __lt__(self: 'Interval', other: 'Interval') -> bool:
        return self.start < other.start

    def print_interval(self: 'Interval') -> None:
        print("[" + str(self.start) + ", " + str(self.end) + "]", end='')


def merge(intervals: List['Interval']) -> List['Interval']:
    if len(intervals) < 2:
        return intervals
    merged = []
    intervals.sort()
    merged.append(intervals[0])
    for i in range(1, len(intervals)):
        if intervals[i].start < merged[-1].end:  # overlap
            merged[-1].end = max(intervals[i].end, merged[-1].end)
        else:
            merged.append(intervals[i])

    return merged


# complexity O(nlgn), dominated by sorting


def main():
    print("Merged intervals: ", end='')
    for i in merge([Interval(1, 4), Interval(2, 5), Interval(7, 9)]):
        i.print_interval()
    print()

    print("Merged intervals: ", end='')
    for i in merge([Interval(6, 7), Interval(2, 4), Interval(5, 9)]):
        i.print_interval()
    print()

    print("Merged intervals: ", end='')
    for i in merge([Interval(1, 4), Interval(2, 6), Interval(3, 5)]):
        i.print_interval()
    print()


main()

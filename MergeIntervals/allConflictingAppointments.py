# Given a list of appointments, find all the conflicting appointments
from typing import List, Tuple

START = 0
END = 1

def is_overlapping(first: List[int], second: List[int]) -> bool:
    if second[START] >= first[END]:
        return False
    return True

def sort_appointments(lst: List[List[int]]) -> List[List[int]]:
    return sorted(lst, key=lambda appt: appt[START])

def all_overlapping(intervals: List[List[int]]) -> List[Tuple[List[int]]]:
    if len(intervals) < 2:
        return []
    intervals = sort_appointments(intervals)
    result = []
    for first in range(len(intervals) - 1):
        for second in range(first + 1, len(intervals)):
            if is_overlapping(intervals[first], intervals[second]):
                result.append((intervals[first], intervals[second]))
    return result

def main():
  print("All overlapping appointments: " + str(all_overlapping([[1, 4], [2, 5], [7, 9]])))
  print("All overlapping appointments: " + str(all_overlapping([[6, 7], [2, 4], [8, 12]])))
  print("All overlapping appointments: " + str(all_overlapping([[4, 5], [2, 3], [3, 6]])))
  print("All overlapping appointments: " + str(all_overlapping([[4, 5]])))
  print("All overlapping appointments: " + str(all_overlapping([[4, 5], [2, 3], [5, 6]])))
  print("All overlapping appointments: " + str(all_overlapping([[4,5], [2,3], [3,6], [5,7], [7,8]])))


main()


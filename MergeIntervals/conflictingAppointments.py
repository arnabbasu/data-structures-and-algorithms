# Given an array of intervals representing ‘N’ appointments, find out if a person can attend all the appointments
from typing import List

START = 0
END = 1

def is_overlapping(first: List[int], second: List[int]) -> bool:
    if second[START] >= first[END]:
        return False
    return True

def sort_appointments(lst: List[List[int]]) -> List[List[int]]:
    return sorted(lst, key=lambda appt: appt[START])

def can_attend_all_appointments(intervals: List[List[int]]) -> bool:
    if len(intervals) < 2:
        return True
    intervals = sort_appointments(intervals)
    first, second = 0, 1
    while second < len(intervals):
        if is_overlapping(intervals[first], intervals[second]):
            return False
        first = first + 1
        second = second + 1
    return True

def main():
  print("Can attend all appointments: " + str(can_attend_all_appointments([[1, 4], [2, 5], [7, 9]])))
  print("Can attend all appointments: " + str(can_attend_all_appointments([[6, 7], [2, 4], [8, 12]])))
  print("Can attend all appointments: " + str(can_attend_all_appointments([[4, 5], [2, 3], [3, 6]])))
  print("Can attend all appointments: " + str(can_attend_all_appointments([[4, 5]])))
  print("Can attend all appointments: " + str(can_attend_all_appointments([[4, 5], [2, 3], [5, 6]])))


main()

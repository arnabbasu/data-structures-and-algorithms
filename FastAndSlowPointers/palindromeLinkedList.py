# Given the head of a Singly LinkedList, write a method to check if the LinkedList is a palindrome or not.
# Your algorithm should use constant space and the input LinkedList should be in the original form once the algorithm
# is finished. The algorithm should have O(N) time complexity where ‘N’ is the number of nodes in the LinkedList.


class Node:
    def __init__(self, value: int, nxt: 'Node' = None):
        self.value = value
        self.next = nxt

    def print_list(self):
        cur = self
        while cur is not None:
            print(cur.value, end='')
            cur = cur.next
        print()


def find_middle(head: 'Node') -> 'Node':
    slow, fast = head, head.next.next

    while fast is not None and fast.next is not None:
        slow = slow.next
        fast = fast.next.next

    return slow


def reverse_sublist(node: 'Node') -> 'Node':
    prev, cur, nxt = None, node, node.next
    while cur is not None:
        cur.next = prev
        prev, cur = cur, nxt
        if nxt is not None:
            nxt = nxt.next
    return prev


def is_palindromic_linked_list(head: 'Node') -> bool:
    if head.next is None:
        return False
    if head.next.next is None:
        if head.value == head.next.value:
            return True
        else:
            return False

    first_list_last_node = find_middle(head)
    first_list_head = head
    second_list_head = reverse_sublist(first_list_last_node.next)
    first_list_last_node.next = None
    t1 = first_list_head
    t2 = second_list_head
    is_palindrome = True
    while t1 is not None:
        if t1.value != t2.value:
            is_palindrome = False
            break
        t1 = t1.next
        t2 = t2.next

    second_list_head = reverse_sublist(second_list_head)
    first_list_last_node.next = second_list_head

    return is_palindrome


def main():
    head = Node(2)
    head.next = Node(4)
    head.next.next = Node(6)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(2)

    print("Is palindrome: " + str(is_palindromic_linked_list(head)))

    head.next.next.next.next.next = Node(2)
    print("Is palindrome: " + str(is_palindromic_linked_list(head)))


main()

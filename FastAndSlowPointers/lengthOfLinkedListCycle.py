# Given the head of a LinkedList with a cycle, find the length of the cycle.


class Node:
    def __init__(self, value: int, nxt: 'Node' = None):
        self.value = value
        self.next = nxt


def find_cycle_length(head: 'Node'):
    slow, fast = head, head
    while fast is not None and fast.next is not None:
        slow = slow.next
        fast = fast.next.next
        if fast == slow:
            return calculate_length(slow)
    return 0


def calculate_length(slow: 'Node'):
    current = slow.next
    length = 1
    while current != slow:
        current = current.next
        length += 1
    return length


def main():
    head = Node(1)
    head.next = Node(2)
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)
    head.next.next.next.next.next = Node(6)
    head.next.next.next.next.next.next = head.next.next
    print("LinkedList cycle length: " + str(find_cycle_length(head)))

    head.next.next.next.next.next.next = head.next.next.next
    print("LinkedList cycle length: " + str(find_cycle_length(head)))


main()

# Given the head of a Singly LinkedList, write a function to determine if the LinkedList has a cycle in it or not.


class Node:
    def __init__(self, value: int, nxt: 'Node' = None):
        self.value = value
        self.next = nxt


def has_cycle(head):
    if head is None or head.next is None:
        return False
    slow, fast = head, head.next
    while fast.next is not None:
        if fast == slow:
            return True
        slow = slow.next
        fast = fast.next.next
    return False


def main():
    print("LinkedList has cycle: " + str(has_cycle(None)))
    head = Node(1)
    print("LinkedList has cycle: " + str(has_cycle(head)))
    head.next = head
    print("LinkedList has cycle: " + str(has_cycle(head)))
    head.next = Node(2)
    print("LinkedList has cycle: " + str(has_cycle(head)))
    head.next.next = head
    print("LinkedList has cycle: " + str(has_cycle(head)))
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)
    head.next.next.next.next.next = Node(6)
    print("LinkedList has cycle: " + str(has_cycle(head)))

    head.next.next.next.next.next.next = head.next.next
    print("LinkedList has cycle: " + str(has_cycle(head)))

    head.next.next.next.next.next.next = head.next.next.next
    print("LinkedList has cycle: " + str(has_cycle(head)))


main()

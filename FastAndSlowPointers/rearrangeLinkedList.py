# Given the head of a Singly LinkedList, write a method to modify the LinkedList such that the nodes from the second
# half of the LinkedList are inserted alternately to the nodes from the first half in reverse order. So if the
# LinkedList has nodes 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> null, your method should return 1 -> 6 -> 2 -> 5 -> 3 -> 4 -> null.
#
# Your algorithm should not use any extra space and the input LinkedList should be modified in-place.
from typing import Optional


class Node:
    def __init__(self, value: int, nxt: 'Node' = None):
        self.value = value
        self.next = nxt

    def print_list(self):
        temp = self
        while temp is not None:
            print(str(temp.value) + " ", end='')
            temp = temp.next
        print()


def find_middle(head: 'Node') -> 'Node':
    slow, fast = head, head.next.next
    while fast is not None and fast.next is not None:
        slow = slow.next
        fast = fast.next.next
    if fast is not None:
        slow = slow.next
    return slow


def reverse_list(head: 'Node') -> Optional['Node']:
    if head is None or head.next is None:
        return head
    prev, cur, nxt = None, head, head.next
    while cur is not None:
        cur.next = prev
        prev = cur
        cur = nxt
        if nxt is not None:
            nxt = nxt.next
    return prev


def merge_lists(first: 'Node', second: 'Node') -> None:
    while second is not None:
        tmp = second
        second = second.next
        tmp.next = first.next
        first.next = tmp
        first = tmp.next
    return


def reorder(head: 'Node') -> None:
    if head is None or head.next is None or head.next.next is None:
        return
    first_list_last = find_middle(head)
    second_list_head = first_list_last.next
    first_list_last.next = None
    second_list_head = reverse_list(second_list_head)
    merge_lists(head, second_list_head)

    return


def main():
    head = Node(2)
    head.next = Node(4)
    head.next.next = Node(6)
    head.next.next.next = Node(8)
    head.next.next.next.next = Node(10)
    head.next.next.next.next.next = Node(12)
    reorder(head)
    head.print_list()


main()

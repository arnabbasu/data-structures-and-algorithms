# Any number will be called a happy number if, after repeatedly replacing it with a number equal to the sum of the
# square of all of its digits, leads us to number ‘1’. All other (not-happy) numbers will never reach ‘1’. Instead,
# they will be stuck in a cycle of numbers which does not include ‘1’.


def calc_square_sum(num: int) -> int:
    _sum = 0
    while num:
        digit = num % 10
        _sum += (digit * digit)
        num //= 10
    return _sum


# O(n) extra space
def find_happy_number1(num: int) -> bool:
    seen = {}
    sq_sum = num
    while True:
        sq_sum = calc_square_sum(sq_sum)
        if sq_sum == 1:
            return True
        elif sq_sum in seen:
            return False
        else:
            seen[sq_sum] = 1


# O(1) extra space
def find_happy_number2(num: int) -> bool:
    slow = calc_square_sum(num)
    fast = calc_square_sum(calc_square_sum(num))

    while fast != slow:
        if fast == 1:
            return True
        slow = calc_square_sum(slow)
        fast = calc_square_sum(calc_square_sum(fast))
    return False


# Time complexity is O(log n) [too mathematical to remember]


def main():
    print(find_happy_number1(23))
    print(find_happy_number1(12))
    print(find_happy_number2(23))
    print(find_happy_number2(12))


main()

# Given the head of a Singly LinkedList that contains a cycle, write a function to find the starting node of the cycle.
from typing import Optional


class Node:
    def __init__(self, value: int, nxt: 'Node' = None):
        self.value = value
        self.next = nxt

    def print_list(self):
        temp = self
        while temp is not None:
            print(temp.value, end='')
            temp = temp.next
        print()


def find_cycle_start(head: 'Node') -> Optional['Node']:
    cycle_length = find_cycle_length(head)
    if cycle_length == 0:
        return None
    slow, fast = head, head

    while cycle_length > 0:
        fast = fast.next
        cycle_length -= 1

    while fast != slow:
        slow = slow.next
        fast = fast.next

    return fast


def find_cycle_length(head: 'Node') -> int:
    slow, fast = head, head
    while fast is not None and fast.next is not None:
        slow = slow.next
        fast = fast.next.next
        if fast == slow:
            return get_cycle_length(slow)
    return 0


def get_cycle_length(slow: 'Node') -> int:
    current = slow.next
    length = 1
    while current != slow:
        current = current.next
        length += 1
    return length


def main():
    head = Node(1)
    head.next = Node(2)
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)
    head.next.next.next.next.next = Node(6)
    head.print_list()

    head.next.next.next.next.next.next = head.next.next
    print("LinkedList cycle start: " + str(find_cycle_start(head).value))

    head.next.next.next.next.next.next = head.next.next.next
    print("LinkedList cycle start: " + str(find_cycle_start(head).value))

    head.next.next.next.next.next.next = head
    print("LinkedList cycle start: " + str(find_cycle_start(head).value))


main()

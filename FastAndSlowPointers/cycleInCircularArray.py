# We are given an array containing positive and negative numbers. Suppose the array contains a number ‘M’ at a
# particular index. Now, if ‘M’ is positive we will move forward ‘M’ indices and if ‘M’ is negative move backwards
# ‘M’ indices. You should assume that the array is circular which means two things:
#
# 1. If, while moving forward, we reach the end of the array, we will jump to the first element to continue the
# movement.
# 2. If, while moving backward, we reach the beginning of the array, we will jump to the last element to continue the
# movement.
# Write a method to determine if the array has a cycle. The cycle should have more than one element and should follow
# one direction which means the cycle should not contain both forward and backward movements.
from typing import List


# O(n^2) time and O(1) space:
# search for loop from each index, stop search if:
# 1. element points to itself
# 2. if there is a change if direction after starting from that element
# if fast and slow pointers meet then there is a loop


def circular_array_loop_exists(arr: List[int]) -> bool:
    print(arr)
    for i in range(len(arr)):
        initially_moving_forward = (arr[i] >= 0)
        slow, fast = i, i
        print("==")
        while True:
            print("slow: {}, fast: {}".format(slow, fast))
            slow = get_next_index(arr, initially_moving_forward, slow)
            if slow == -1:
                break
            fast = get_next_index(arr, initially_moving_forward, fast)
            if fast == -1:
                break
            fast = get_next_index(arr, initially_moving_forward, fast)
            if fast != -1 and fast == slow:
                print("slow: {}, fast: {}".format(slow, fast))
                return True
    return False


def get_next_index(arr: List[int], initially_moving_forward: bool, start_index: int) -> int:
    currently_moving_forward = (arr[start_index] >= 0)
    if initially_moving_forward != currently_moving_forward:
        return -1
    next_index = (start_index + arr[start_index]) % len(arr)
    if start_index == next_index:
        return -1
    return next_index


# O(n) time and O(n) space:
# search for loop from each index, stop search if:
# 1. element points to itself
# 2. if there is a change if direction after starting from that element
# if fast and slow pointers meet then there is a loop
# we store indexes that have been evaluated and rejected already, and if current trip takes us to such an element
# we stop the search


def main():
    print(circular_array_loop_exists([1, 2, -1, 2, 2]))
    print(circular_array_loop_exists([2, 2, -1, 2]))
    print(circular_array_loop_exists([2, 1, -1, -2]))
    print(circular_array_loop_exists([2, 0, -1, 2]))


main()

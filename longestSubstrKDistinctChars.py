# Given a string, find the length of the longest substring in it with no more than K distinct characters.


def longest_substr_k_distinct_chars(k, st):
    max_length, window_start, chars_present = 0, 0, {}  # chars_present is the dict in which we track chars encountered
    for window_end in range(len(st)):
        cur_char = st[window_end]
        # add a new char to the dictionary or increase the count
        if cur_char not in chars_present:
            chars_present[cur_char] = 1
        else:
            chars_present[cur_char] += 1

        # while there are too many distinct chars
        while len(chars_present) > k:
            # shrink the window from the left and reduce the count for the dropped char
            left_char = st[window_start]
            chars_present[left_char] -= 1
            if chars_present[left_char] == 0:
                del chars_present[left_char]
            window_start += 1

        max_length = max(max_length, window_end - window_start + 1)  # at each end pos, record the max length

    return max_length


def main():
    print("Length of the longest substring: " + str(longest_substr_k_distinct_chars(2, "araaci")))
    print("Length of the longest substring: " + str(longest_substr_k_distinct_chars(1, "araaci")))
    print("Length of the longest substring: " + str(longest_substr_k_distinct_chars(3, "cbbebi")))


main()

# Given a binary tree, return an array containing nodes in its left view. The left view of a binary tree is the set of
# nodes visible when the tree is seen from the left side

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def tree_left_view(root: 'TreeNode') -> List['TreeNode']:
    result = []
    if root is None:
        return result

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)

    while nodes:
        num_nodes_at_level = len(nodes)
        result.append(nodes[0])
        while num_nodes_at_level:
            node = nodes.popleft()
            num_nodes_at_level = num_nodes_at_level - 1
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    root.left.left.left = TreeNode(3)
    result = tree_left_view(root)
    print("Tree left view: ")
    for node in result:
        print(str(node.val) + " ", end='')


main()

# Given a binary tree and a node, find the level order successor of the given node in the tree. The level order
# successor is the node that appears right after the given node in the level order traversal.


from collections import deque
from typing import Optional


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def find_successor(root: 'TreeNode', key: int) -> Optional['TreeNode']:
    if not root:
        return None

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)

    while nodes:
        node = nodes.popleft()
        if node.left:
            nodes.append(node.left)
        if node.right:
            nodes.append(node.right)
        if node.val == key:
            break
    return nodes[0] if nodes else None


def main():
    root = TreeNode(12)
    result = find_successor(root, 12)
    if result:
        print(result.val)
    else:
        print('None')
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    result = find_successor(root, 12)
    if result:
        print(result.val)
    result = find_successor(root, 9)
    if result:
        print(result.val)


main()

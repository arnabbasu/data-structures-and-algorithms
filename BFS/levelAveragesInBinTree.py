# Given a binary tree, populate an array to represent the averages of all of its levels.

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def find_level_averages(root: 'TreeNode') -> List[float]:
    result = []
    if not root:
        return result

    nodes = deque()
    nodes.append(root)
    while nodes:
        nodes_at_level = len(nodes)
        average = 1.0 / nodes_at_level
        node_sum = 0
        while nodes_at_level != 0:
            node = nodes.popleft()
            nodes_at_level = nodes_at_level - 1
            node_sum = node_sum + node.val
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
        result.append(node_sum * average)
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.left.right = TreeNode(2)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Level averages are: " + str(find_level_averages(root)))


main()

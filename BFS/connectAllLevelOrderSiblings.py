# Given a binary tree, connect each node with its level order successor. The last node of each level should point to
# the first node of the next level

from collections import deque
from typing import Optional


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right, self.next = None, None, None

    # tree traversal using 'next' pointer
    def print_tree(self: 'TreeNode') -> None:
        print("Traversal using 'next' pointer: ", end='')
        current = self
        while current:
            print(str(current.val) + " ", end='')
            current = current.next


def connect_all_siblings(root: 'TreeNode') -> None:
    if root is None:
        return

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)
    prev_node: Optional['TreeNode'] = None

    while nodes:
        cur_node = nodes.popleft()
        if prev_node:
            prev_node.next = cur_node
        prev_node = cur_node
        if cur_node.left:
            nodes.append(cur_node.left)
        if cur_node.right:
            nodes.append(cur_node.right)
    return


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    connect_all_siblings(root)
    root.print_tree()


main()

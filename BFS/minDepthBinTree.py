# Find the minimum depth of a binary tree. The minimum depth is the number of nodes along the shortest path from the
# root node to the nearest leaf node.

from collections import deque


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def find_minimum_depth(root: 'TreeNode') -> int:
    if not root:
        return 0

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)
    depth = 1

    while nodes:
        nodes_at_level = len(nodes)
        while nodes_at_level != 0:
            node = nodes.popleft()
            nodes_at_level = nodes_at_level - 1
            if not node.left and not node.right:
                return depth
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
        depth = depth + 1


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Tree Minimum Depth: " + str(find_minimum_depth(root)))
    root.left.left = TreeNode(9)
    root.right.left.left = TreeNode(11)
    print("Tree Minimum Depth: " + str(find_minimum_depth(root)))


main()

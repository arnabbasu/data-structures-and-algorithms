# Given a binary tree, populate an array to represent its level-by-level traversal. You should populate the values of
# all nodes of each level from left to right in separate sub-arrays.

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def traverse(root: 'TreeNode') -> List[List['TreeNode']]:
    result = []
    if not root:
        return result

    nodes: deque['TreeNode'] = deque()

    nodes.append(root)
    while nodes:
        nodes_at_this_level = len(nodes)
        result.append([])
        while nodes_at_this_level != 0:
            node = nodes.popleft()
            nodes_at_this_level = nodes_at_this_level - 1
            result[-1].append(node.val)
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Level order traversal: " + str(traverse(root)))


main()

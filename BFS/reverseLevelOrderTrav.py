# Given a binary tree, populate an array to represent its level-by-level traversal in reverse order, i.e., the lowest
# level comes first. You should populate the values of all nodes in each level from left to right in separate
# sub-arrays.

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def traverse(root: 'TreeNode') -> List[List['TreeNode']]:
    result = []
    if not root:
        return result

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)
    while nodes:
        level_size = len(nodes)
        result.insert(0, [])
        while level_size != 0:
            node = nodes.popleft()
            level_size = level_size - 1
            result[0].append(node.val)
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Reverse level order traversal: " + str(traverse(root)))


main()

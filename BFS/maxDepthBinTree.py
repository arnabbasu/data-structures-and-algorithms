# Given a binary tree, find its maximum depth (or height).

from collections import deque


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def find_maximum_depth(root: 'TreeNode') -> int:
    if not root:
        return 0

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)
    depth = 0

    while nodes:
        depth = depth + 1
        nodes_at_depth = len(nodes)
        while nodes_at_depth != 0:
            node = nodes.popleft()
            nodes_at_depth = nodes_at_depth - 1
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    return depth


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    print("Tree Maximum Depth: " + str(find_maximum_depth(root)))
    root.left.left = TreeNode(9)
    root.right.left.left = TreeNode(11)
    print("Tree Maximum Depth: " + str(find_maximum_depth(root)))


main()

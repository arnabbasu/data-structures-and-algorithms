# Given a binary tree, connect each node with its level order successor. The last node of each level should point to a
# null node.

from collections import deque


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right, self.next = None, None, None

    # level order traversal using 'next' pointer
    def print_level_order(self: 'TreeNode') -> None:
        next_level_root = self
        while next_level_root:
            current = next_level_root
            next_level_root = None
            while current:
                print(str(current.val) + " ", end='')
                if not next_level_root:
                    if current.left:
                        next_level_root = current.left
                    elif current.right:
                        next_level_root = current.right
                current = current.next
            print()


def connect_level_order_siblings(root: 'TreeNode') -> None:
    if root is None:
        return

    nodes: deque['TreeNode'] = deque()
    nodes.append(root)

    while nodes:
        num_nodes_at_level = len(nodes)
        while num_nodes_at_level != 0:
            node = nodes.popleft()
            num_nodes_at_level = num_nodes_at_level - 1
            if num_nodes_at_level == 0:
                node.next = None
            else:
                node.next = nodes[0]
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    return


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    connect_level_order_siblings(root)

    print("Level order traversal using 'next' pointer: ")
    root.print_level_order()


main()

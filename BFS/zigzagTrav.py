# Given a binary tree, populate an array to represent its zigzag level order traversal. You should populate the values
# of all nodes of the first level from left to right, then right to left for the next level and keep alternating in
# the same manner for the following levels

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def traverse(root: 'TreeNode') -> List[List[int]]:
    result = []
    if not root:
        return result

    nodes: deque['TreeNode'] = deque()
    left_to_right = True
    nodes.append(root)

    while nodes:
        nodes_at_level = len(nodes)
        current_level: deque[int] = deque()
        while nodes_at_level != 0:
            node = nodes.popleft()
            nodes_at_level = nodes_at_level - 1
            if left_to_right:
                current_level.append(node.val)
            else:
                current_level.appendleft(node.val)
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
        result.append(list(current_level))
        left_to_right = not left_to_right
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(9)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    root.right.left.left = TreeNode(20)
    root.right.left.right = TreeNode(17)
    print("Zigzag traversal: " + str(traverse(root)))


main()

# Given a binary tree, return an array containing all the boundary nodes of the tree in an anti-clockwise direction.
#
# The boundary of a tree contains all nodes in the left view, all leaves, and all nodes in the right view. Please note
# that there should not be any duplicate nodes. For example, the root is only included in the left view; similarly,
# if a level has only one node we should include it in the left view.

from collections import deque
from typing import List


class TreeNode:
    def __init__(self: 'TreeNode', val: int):
        self.val = val
        self.left, self.right = None, None


def find_leaves(root: 'TreeNode') -> List['TreeNode']:
    result: List['TreeNode'] = []
    if root is None:
        return result
    node_stack: deque['TreeNode'] = deque()
    node_stack.append(root)
    while node_stack:
        node = node_stack.pop()
        if node.left is None and node.right is None:
            result.append(node)
        if node.right:
            node_stack.append(node.right)
        if node.left:
            node_stack.append(node.left)
    return result


def find_tree_boundary(root: 'TreeNode') -> List['TreeNode']:
    result: List['TreeNode'] = []
    if root is None:
        return result

    right_view: List['TreeNode'] = []
    nodes: deque['TreeNode'] = deque()
    nodes.append(root)

    while nodes:
        result.append(nodes[0])
        num_nodes_at_level = len(nodes)
        while num_nodes_at_level != 0:
            node = nodes.popleft()
            num_nodes_at_level = num_nodes_at_level - 1
            if num_nodes_at_level == 0 and result[-1] != node:
                right_view.insert(0, node)
            if node.left:
                nodes.append(node.left)
            if node.right:
                nodes.append(node.right)
    result.extend(find_leaves(root)[1:-1])
    result.extend(right_view)
    return result


def main():
    root = TreeNode(12)
    root.left = TreeNode(7)
    root.right = TreeNode(1)
    root.left.left = TreeNode(4)
    root.left.left.left = TreeNode(9)
    root.left.right = TreeNode(3)
    root.left.right.left = TreeNode(15)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    root.right.right.left = TreeNode(6)
    result = find_tree_boundary(root)
    print("Tree boundary: ", end='')
    for node in result:
        print(str(node.val) + " ", end='')
    print()
    root = TreeNode(12)
    root.right = TreeNode(1)
    root.right.left = TreeNode(10)
    root.right.right = TreeNode(5)
    result = find_tree_boundary(root)
    print("Tree boundary: ", end='')
    for node in result:
        print(str(node.val) + " ", end='')
    print()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(5)
    root.right.left = TreeNode(6)
    root.right.right = TreeNode(7)
    root.left.left.left = TreeNode(8)
    root.left.left.right = TreeNode(9)
    root.left.right.left = TreeNode(10)
    root.left.right.right = TreeNode(11)
    root.right.left.left = TreeNode(12)
    root.right.left.right = TreeNode(13)
    root.right.right.left = TreeNode(14)
    root.right.right.right = TreeNode(15)
    result = find_tree_boundary(root)
    print("Tree boundary: ", end='')
    for node in result:
        print(str(node.val) + " ", end='')


main()

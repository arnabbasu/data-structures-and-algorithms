# Given an array of numbers find the maximum sum of any contiguous subarray
# Kaden's Algorithm


def max_sum_subarray(arr):
    max_at_current_idx, global_max = arr[0], arr[0]
    for index in range(1, len(arr)):
        max_at_current_idx = max(arr[index],
                                 max_at_current_idx + arr[index])  # max at curr is either curr or cur + max at prev
        global_max = max(global_max, max_at_current_idx)

    return global_max


def main():
    print("Maximum sum of a subarray: " + str(max_sum_subarray([2, 1, 5, 1, 3, 2])))
    print("Maximum sum of a subarray: " + str(max_sum_subarray([-2, 3, 2, -1])))
    print("Maximum sum of a subarray: " + str(max_sum_subarray([-4, -3, -2, -1])))


main()

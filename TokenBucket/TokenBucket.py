from threading import Lock
from time import time_ns


class TokenBucket:
    def __init__(self, capacity: int, refill_rate: int):
        self._lock = Lock()

        self._capacity = capacity
        self._refill_rate = refill_rate  # refill rate in tokens per second

        self._current_level = self._capacity
        self._last_filled_time = time_ns()

    def allow_request(self, tokens: int) -> bool:
        with self._lock:
            self._refill()

            if self._current_level >= tokens:
                self._current_level -= tokens
                return True

            return False

    def _refill(self):
        now: int = time_ns()
        tokens_to_add: int = ((now - self._last_filled_time) * self._refill_rate) // 1000000000
        self._current_level = min(self._current_level + tokens_to_add, self._capacity)
        self._last_filled_time = now

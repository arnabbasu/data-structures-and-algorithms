from __future__ import annotations  # postponed evaluation of annotations
from abc import ABC, abstractmethod
from typing import Optional, Iterator


class Tree(ABC):
    """ An Abstract base class representing a tree"""

    # inner class representing a position
    class Position(ABC):
        """ An Abstract base class representing the position of a single element"""

        @abstractmethod
        def element(self):
            """ Return the element stored at this position"""
            pass

        @abstractmethod
        def __eq__(self, other: Tree.Position) -> bool:
            """ Return True of the other position represents the same location"""
            pass

        def __ne__(self, other: Tree.Position) -> bool:
            """ Returns True if other does not represent the same location"""
            return not (self == other)

    @abstractmethod
    def root(self) -> Optional[Tree.Position]:
        """ return Position representing the Tree's root or None if empty"""
        pass

    @abstractmethod
    def parent(self, p: Tree.Position) -> Optional[Tree.Position]:
        """ return Position representing the parent of p in this Tree, None if root"""
        pass

    @abstractmethod
    def num_children(self, p: Tree.Position) -> int:
        """ return the number of children of that Position p has"""
        pass

    @abstractmethod
    def children(self, p: Tree.Position) -> Iterator[Tree.Position]:
        """ Generate an iteration of Positions representing p's children"""
        pass

    @abstractmethod
    def __len__(self) -> int:
        """ return the total number of elements in the tree"""
        pass

    def is_root(self, p: Tree.Position) -> bool:
        """ return True of Position p is the root of the Tree"""
        return self.root() == p

    def is_leaf(self, p: Tree.Position) -> bool:
        """return True if Position p does not have any children"""
        return self.num_children(p) == 0

    def is_empty(self) -> bool:
        """Return True is Tree is empty"""
        return len(self) == 0

    def depth(self, p: Tree.Position) -> int:
        """ return the number of levels separating p from root"""
        if self.is_root(p):
            return 0
        else:
            return 1 + self.depth(self.parent(p))

    def _height(self, p: Tree.Position) -> int:
        """ return the height of the sub tree rooted at p,"""
        if self.is_leaf(p):
            return 0
        else:
            return 1 + max(self._height(c) for c in self.children(p))

    def height(self, p: Optional[Tree.Position] = None) -> int:
        """ return the height of the subtree rooted at p, return the height of the tree if p is None"""
        if not p:
            p = self.root()
        return self._height(p)

from __future__ import annotations  # postponed evaluation of annotations
from abc import abstractmethod
from typing import Optional, Iterator
from Tree import Tree


class BinaryTree(Tree):
    """ ABC representing a Binary Tree data structure"""

    @abstractmethod
    def left(self, p: Tree.Tree.Position) -> Optional[Tree.Tree.Position]:
        """ returns the left child of p or None if p does not have a left child"""
        pass

    @abstractmethod
    def right(self, p: Tree.Tree.Position) -> Optional[Tree.Tree.Position]:
        """ returns the right child of p or None if p does not have a right child"""
        pass

    def sibling(self, p: Tree.Tree.Position) -> Optional[Tree.Tree.Position]:
        """ return the sibling position of p or None """
        parent = self.parent(p)
        if parent is None:  # root has no siblings
            return None
        elif p == self.left(parent):
            return self.right(parent)  # possibly None
        else:
            return self.left(parent)  # possibly None

    def children(self, p: Tree.Tree.Position) -> Iterator[Tree.Tree.Position]:
        """ generate an iteration of positions representing p's children"""
        if self.left(p):
            yield self.left(p)
        if self.right(p):
            yield self.right(p)
